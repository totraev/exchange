const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const YAML = require('yamljs');

const ROOT_DIR = path.resolve(__dirname, '../../');
const DIST_DIR = path.resolve(ROOT_DIR, 'dist');
const SRC_DIR = path.resolve(ROOT_DIR, 'src');
const VENDOR_DIR = path.resolve(ROOT_DIR, 'vendor');

module.exports = {
  entry: [SRC_DIR],

  resolve: {
    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
    modules: [SRC_DIR, 'node_modules'],
    plugins: [new TsconfigPathsPlugin({})]
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        SERVER_URL: JSON.stringify(process.env.SERVER_URL)
      }
    }),
    new CopyWebpackPlugin([{
      from: VENDOR_DIR + '/tradingView/static',
      to: DIST_DIR + '/tradingView/static'
    }, {
      from: ROOT_DIR + '/locales/**/*.yml',
      to: DIST_DIR + '/[path]/[name].json',
      transform: content => JSON.stringify(YAML.parse(content.toString())),
      toType: 'template'
    }])
  ],

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true
            }
          }
        ]
      },
      {
        test:   /\.(ttf|otf|eot|svg|woff2?)(\?.+)?$/,
        loader: 'url-loader',
        options:  {
          limit: 10000
        }
      },
      {
        test: /\.(jpe?g|png|gif|ico)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]'
        }
      }
    ]
  }
};
