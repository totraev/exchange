import WebSocketClient from './WebSocketClient';
import RootStore from '../stores/RootStore';

import { L3Message } from './types/types';


class L3UpdatesClient extends WebSocketClient {
  constructor(private rootStore: RootStore) {
    super('ws_l3_user_updates', rootStore.authStore.authData);

    this.on('message', this.processMessage);
    this.on('error', (data) => {
      this.rootStore.openOrdersStore.stopLoading();
      console.log(data);
    });
  }

  private processMessage = (msg: L3Message) => {
    switch (msg.type) {
      case 'l3update':
        return this.rootStore.openOrdersStore.handleMessage(msg);
      case 'snapshot':
        this.rootStore.openOrdersStore.stopLoading();
        return this.rootStore.openOrdersStore.handleSnapshot(msg.payload);
    }
  }
}

export default L3UpdatesClient;
