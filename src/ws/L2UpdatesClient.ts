import WebSocketClient from './WebSocketClient';
import RootStore from '../stores/RootStore';
import throttle from 'lodash.throttle';

import { L2Message, L2SnapshotMessage, L2UpdateMessage, OrderInfo } from './types/types';

class L2UpdatesClient extends WebSocketClient {
  private currPair: string = '';
  private chunk: OrderInfo[] = [];

  constructor(private rootStore: RootStore) {
    super('ws_l2_updates');

    this.on('message', this.processMessage);
    this.on('error', data => console.log(data));
  }

  subscribe(currPair: string) {
    if (Boolean(currPair)) {
      this.unsubscribe();
      this.currPair = currPair;
      this.rootStore.orderBook.setCurrPair(currPair);
      this.send({ type: 'subscribe', payload: [currPair] });
    }
  }

  unsubscribe() {
    this.send({ type: 'unsubscribe', payload: [this.currPair] });
    this.currPair = '';
  }

  private processMessage = (msg: L2Message) => {
    switch (msg.type) {
      case 'l2update':
        return this.updateOrderBook(msg);
      case 'snapshot':
        return this.initOrderBook(msg);
    }
  }

  private initOrderBook({ payload }: L2SnapshotMessage) {
    this.rootStore.orderBook.updateOrders(payload);
    this.rootStore.appStore.loadEnd();
  }

  private updateOrderBook({ payload }: L2UpdateMessage) {
    this.chunk.push(payload);
    this.syncOrderBook();
  }

  private syncOrderBook = throttle(() => {
    this.rootStore.orderBook.updateOrders(this.chunk);
    this.chunk = [];
  }, 100);
}

export default L2UpdatesClient;
