import BigNumber from 'bignumber.js';

export type AuthData = {
  apikey: string
  secret: string
};

// L2 Update messages
export type Order = {
  id: string
  side: 'buy' | 'sell'
  price: BigNumber
  size: BigNumber
  timestamp: number
};

export type OrderInfo = {
  seq_num: number,
  curr_pair: string,
  side: 'sell' | 'buy',
  price: string,
  volume: string,
  count: number,
  timestamp: number
};

export type L2SnapshotMessage = {
  type: 'snapshot'
  payload: OrderInfo[]
};

export type L2UpdateMessage = {
  type: 'l2update'
  payload: OrderInfo
};

export type L2Message = L2SnapshotMessage | L2UpdateMessage;


// L3 Update Messages
export type L3OrderCreatedPayload = {
  action: 1
  curr_pair: string
  order_id: number
  side: 'buy' | 'sell'
  type: 'limit' | 'market'
  price: string
  volume: string
  original_volume: string
  event_time: number
};

export type L3OrderExecutedPayload = {
  action: 2
  curr_pair: string
  order_id: number
  volume: string
  original_volume: string
  event_time: number
};

export type L3OrderCanceledPayload = {
  action: 3
  curr_pair: string
  order_id: number
  event_time: number
};

export type L3UpdateMessage = {
  type: 'l3update'
  payload: L3OrderCanceledPayload | L3OrderCreatedPayload | L3OrderExecutedPayload
};

export type L3SnapshotMessage = {
  type: 'snapshot'
  payload: L3OrderCreatedPayload[]
};

export type L3Message = L3SnapshotMessage | L3UpdateMessage;
