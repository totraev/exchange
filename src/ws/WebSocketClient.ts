import { EventEmitter } from 'events';
import cfg from '../config';

import hmacSHA512 from 'crypto-js/hmac-sha512';
import Base64 from 'crypto-js/enc-base64';
import Hex from 'crypto-js/enc-hex';
import Utf8 from 'crypto-js/enc-utf8';

import { AuthData } from './types/types';

class WebSocketClient extends EventEmitter {
  protected socket: WebSocket = null;

  constructor(private wsPath: string, private auth: AuthData = null) {
    super();
  }

  connect() {
    if (this.socket) {
      this.socket.close();
    }

    const protocol = cfg.protocol === 'http' ? 'ws' : 'wss';
    const websocketURI = `${protocol}://${cfg.hostname}/${cfg.api_version}/${this.wsPath}`;

    this.socket = new WebSocket(websocketURI);

    this.socket.onmessage = this.handleMessage;
    this.socket.onopen = this.handleOpen;
    this.socket.onclose = this.handleClose;
    this.socket.onerror = this.handleError;
  }

  disconnect() {
    if (!this.socket) {
      throw new Error('Could not disconnect (not connected)');
    }

    this.socket.close();
    this.socket = null;
  }

  get state(): number {
    return this.socket.readyState;
  }
  
  send(msg: any) {
    this.socket.send(JSON.stringify(msg));
  }

  private authenticate() {
    const data = {
      request: `/${cfg.api_version}/${this.wsPath}`,
      timestamp: Date.now() * 1000 * 1000
    };

    const payload = Base64.stringify(Utf8.parse(JSON.stringify(data)));
    const signature = Hex.stringify(hmacSHA512(payload, this.auth.secret));

    this.send({ 
      payload,
      signature,
      type: 'auth',
      apikey: this.auth.apikey
    });
  }

  private handleOpen = () => {
    this.emit('open');

    if (this.auth !== null) {
      this.authenticate();
    }
  }

  private handleClose = (data: CloseEvent) => {
    this.socket = null;
    this.emit('close', data);
    const { wasClean } = data;

    if (!wasClean) {
      this.emit('error', data);
    }
  }

  private handleMessage = ({ data }: MessageEvent) => {
    const message = JSON.parse(data);

    if (message.type === 'error') {
      this.emit('error', message);
    } else {
      this.emit('message', message);
    }
  }

  private handleError = (data) => {
    this.emit('error', data);
  }
}

export default WebSocketClient;
