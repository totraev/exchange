import React from 'react';
import OrderTable from '../../../utils/OrderTable';

import { Order } from '../../../ws/types/types';
import BigNumber from 'bignumber.js';

export type Props = {
  orders: Order[]
  userOrders: Map<string, BigNumber>
  initialRenderHandler: () => void
};


export default class OrderCanvas extends React.PureComponent<Props> {
  private canvas: HTMLCanvasElement = null;
  private orderTable: OrderTable = null;

  componentDidMount() {
    this.orderTable = new OrderTable(
      this.canvas,
      this.props.initialRenderHandler,
      350,
      0
    );
  }

  componentDidUpdate() {
    this.orderTable.draw(this.props.orders);
    this.orderTable.setUserOrdersMap(this.props.userOrders);
  }

  render() {
    return <canvas ref={canvas => this.canvas = canvas}/>;
  }
}

