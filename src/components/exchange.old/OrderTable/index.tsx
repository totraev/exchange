import React from 'react';
import { inject, observer } from 'mobx-react';
import BigNumber from 'bignumber.js';
import distanceInWordsToNow from 'date-fns/distance_in_words_to_now';
import { InjectStoreProps } from '../../../stores/RootStore';
import './styles.sss';

import BlockTitle from '../BlockTitle';
import Loader from '../../common/Loader';


@inject('rootStore') @observer
class OrderTable extends React.Component<InjectStoreProps> {
  handleCancelOrder(currPair: string, orderId: number): React.MouseEventHandler<HTMLButtonElement> {
    return () => {
      this.props.rootStore.openOrdersStore.cancelOrder(currPair, orderId);
    };
  }

  handleCancelAll: React.MouseEventHandler<HTMLButtonElement> = () => {
    this.props.rootStore.openOrdersStore.cancelAllOrders();
  }

  private calcFilledValue(originValue: string, value: string): string {
    return (new BigNumber(originValue))
      .minus(new BigNumber(value))
      .toString();
  }

  private calcDate(nanoseconds: number): string {
    const ms = Math.round(nanoseconds / 1000000);

    return `${distanceInWordsToNow(ms, { includeSeconds: true })} ago`;
  }

  private orderStatus(state: 'open' | 'canceled' | 'filled' | 'partial'): string {
    switch (state) {
      case 'open':
        return 'Open';
      case 'canceled':
        return 'Canceled';
      case 'partial':
        return 'Partial filled';
      case 'filled':
        return 'Filled';
    }
  }

  private renderOrders() {
    const { openOrdersStore, appStore } = this.props.rootStore;
    const [sizeCurr, priceCurr] = appStore.currentCurrencies;

    return openOrdersStore.hasOrders 
      ? openOrdersStore.currentOrders.map(({ curr_pair, order_id, ...order }) => {
        const isActive = order.state === 'open';

        return (
          <tr
            styleName={isActive ? 'open' : 'close'}
            key={`${curr_pair}:${order_id}`}>
            <td>
              <span styleName={order.side} />
            </td>
            <td>
              {order.volume} <b>{sizeCurr}</b>
            </td>
            <td>
              {this.calcFilledValue(order.original_volume, order.volume)} <b>{sizeCurr}</b>
            </td>
            <td>
              {Boolean(order.price)
                ? <>{order.price} <b>{priceCurr}</b></>
                : null}
            </td>
            <td>{this.calcDate(order.timestamp)}</td>
            <td>{this.orderStatus(order.state)}</td>
            <td>
              {isActive
                ? <button
                  onClick={this.handleCancelOrder(curr_pair, order_id)}
                  styleName="cancel"
                  children="Cancel"/>
                : null}
            </td>
          </tr>
        );
      })
    : <tr>
        <td colSpan={7}>You have no {sizeCurr}/{priceCurr} orders</td>
    </tr>;
  }

  render() {
    const { openOrdersStore } = this.props.rootStore;
    const cancelBtn = openOrdersStore.hasOrders
      ? <button
        styleName="cancel-all-btn"
        onClick={this.handleCancelAll}
        children="Cancel all orders"/>
      : null;

    return (
      <div styleName="wrap">
        <BlockTitle append={cancelBtn}>
          Open Orders
        </BlockTitle>

        {!openOrdersStore.loading
          ? (
            <div styleName="container">
              <table styleName="table">
                <thead>
                  <tr>
                    <th></th>
                    <th>Remaining</th>
                    <th>Filled</th>
                    <th>Price</th>
                    <th>Time</th>
                    <th>Status</th>
                    <th></th>
                  </tr>
                </thead>

                <tbody>
                  {this.renderOrders()}
                </tbody>
              </table>
            </div>
          )
          : <Loader />}
      </div>
    );
  }
}

export default OrderTable;
