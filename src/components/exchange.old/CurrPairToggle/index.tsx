import React from 'react';
import './style.sss';
import { Link } from 'react-router-dom';

import FontAwesomeIcon from '@fortawesome/react-fontawesome';

export type Props = {
  children: string[]
  current: string
  handleClick: (currPair: string) => void
};


const CurrPairToggle: React.SFC<Props> = ({ children, current }) => {
  return (
    <div styleName="wrap">
      <button styleName="toggle">
        {current}

        <FontAwesomeIcon styleName="icon--close" icon="angle-down"/>
      </button>

      <ul styleName="dropdown">
        {children
          .filter(currPair => currPair !== current)
          .map(currPair =>
            <li styleName="item" key={`dropdown-${currPair}`}>
              <Link styleName="link" to={`/trade/${currPair}`}>{currPair}</Link>
            </li>)}
      </ul>
    </div>
  );
};

export default CurrPairToggle;
