import React from 'react';
import { translate, InjectedTranslateProps } from 'react-i18next';
import './styles.sss';

export type Props = InjectedTranslateProps;

const Header: React.SFC<Props> = ({ children, t }) => (
  <div styleName="header">
    <a href="/" styleName="brand" title="Wunder Excahnge">
      <img src={require('./images/logo-h.png')} alt="Brand" styleName="logo"/>
      EXCHANGE
    </a>

    <div styleName="navbar">
      {children}
    </div>
    {t('my.key')}
  </div>
);

export default translate('exchange')(Header);
