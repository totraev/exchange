import React from 'react';
import { observer, inject } from 'mobx-react';
import './styles.sss';

import { InjectStoreProps } from '../../../stores/RootStore';


const OrderHeader: React.SFC<InjectStoreProps> = (props) => {
  const { orderBook } = props.rootStore;

  return (
    <div styleName="order-book-header">
      <div styleName="aggr">
        Aggregation:
        <span styleName="aggr-value">{orderBook.aggrValue.toFixed(2)}</span>
      </div>

      <div styleName="aggr-controls">
        <button
          styleName="btn"
          disabled={orderBook.isIncBtnDisabled}
          onClick={orderBook.incAggr}>
          +
        </button>
        <button
          styleName="btn"
          disabled={orderBook.isDecBtnDisabled}
          onClick={orderBook.decAggr}>
          -
        </button>
      </div>
    </div>
  );
};

export default inject('rootStore')(observer(OrderHeader));
