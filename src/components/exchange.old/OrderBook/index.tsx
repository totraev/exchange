import React from 'react';
import { observer, inject } from 'mobx-react';
import { hot } from 'react-hot-loader';
import './styles.sss';

import { InjectStoreProps } from '../../../stores/RootStore';

import OrderCanvas from '../OrderCanvas';
import BlockTitle from '../BlockTitle';
import Loader from '../../common/Loader';
import OrderAggregation from '../OrderAggregation';


@inject('rootStore') @observer
class OrderBook extends React.Component<InjectStoreProps> {
  state = {
    asks: false,
    bids: false
  };
  private tableWrap: HTMLDivElement;
  private tableContainer: HTMLDivElement;

  componentDidUpdate() {
    const { appStore } = this.props.rootStore;
    const { asks, bids } = this.state;

    if (!asks && !bids) {
      const containerHeight = this.tableContainer.clientHeight;
      const wrapHeight = this.tableWrap.clientHeight;

      this.tableWrap.scrollTop = (containerHeight - wrapHeight) / 2;
    }

    if (appStore.loading && asks && bids) {
      this.setState({
        asks: false,
        bids: false
      });
    }
  }

  private asksRendered = () => this.setState({ asks: true });
  private bidsRendered = () => this.setState({ bids: true });

  render() {
    const { appStore, orderBook, openOrdersStore } = this.props.rootStore;

    return (
      <div styleName="order-book">
        <BlockTitle>OrderBook</BlockTitle>

        <div styleName="table-head">
          <div>Size</div>
          <div>Price</div>
          <div>My size</div>
        </div>
        
        <div styleName="table-wrap" ref={el => this.tableWrap = el}>
          <div
            styleName={appStore.loading ? 'table-container' : ''}
            ref={el => this.tableContainer = el}>
            {appStore.loading 
              ? <Loader /> 
              : <OrderCanvas
                  orders={orderBook.asks}
                  initialRenderHandler={this.asksRendered}
                  userOrders={openOrdersStore.limitAskOrders}/>}

            <div styleName="spread">
              Spread:
              <span styleName="spread-value">
                {orderBook.spread}
              </span>
            </div>
            
            {appStore.loading
              ? <Loader />
              : <OrderCanvas
                  orders={orderBook.bids}
                  initialRenderHandler={this.bidsRendered}
                  userOrders={openOrdersStore.limitBidOrders}/>}
          </div>
        </div>

        <OrderAggregation />
      </div>
    );
  }
}

export default hot(module)(OrderBook);
