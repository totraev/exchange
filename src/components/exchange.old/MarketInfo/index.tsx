import React from 'react';
import './styles.sss';


const MarketInfo: React.SFC = () => (
  <ul styleName="wrap">
    <li styleName="stat">
      7,782.32
      <span styleName="currency">USD</span>
    </li>
    <li styleName="price">
      <span styleName="down">-1.92 %</span>
    </li>
    <li styleName="volume">
      11,245
      <span styleName="currency">BTC</span>
    </li>
  </ul>
);

export default MarketInfo;
