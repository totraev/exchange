import React from 'react';
import './styles.sss';

import BlockTitle from '../BlockTitle';
import TradingView from '../TradingView';


const CandleStick: React.SFC = () => (
  <div styleName="wrap">
    <BlockTitle>PRICE CHART</BlockTitle>

    <TradingView styleName="chart"/>
  </div>
);

export default CandleStick;
