import React from 'react';
import { inject, observer } from 'mobx-react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import './styles.sss';

import { InjectStoreProps as Props } from '../../../stores/RootStore';
import Loader from '../../common/Loader';


@inject('rootStore') @observer
class BalanceInfo extends React.Component<Props> {
  render() {
    const { balanceStore, appStore } = this.props.rootStore;

    return (
      <div styleName="wrap">
        <div styleName="header">BALANCE</div>

        <div styleName="accounts">
          <div styleName="currencies">
            {appStore.currentCurrencies.map((curr, i) => 
              <div styleName="currency-item" key={`balance-${i}`}>{curr}</div>)}
          </div>

          <div styleName="balances">
            {balanceStore.loading
              ? <Loader />
              : balanceStore.currentCurrBalances.map(({ balance }, i) => 
                <div styleName="balance-item" key={`balance-value-${i}`}>{balance}</div>)}
          </div>
        </div>

        <div styleName="buttons">
          <button styleName="btn">
            <FontAwesomeIcon
              styleName="icon--color"
              icon={['far', 'arrow-alt-circle-down']}/>

            Deposite
          </button>
          <button styleName="btn">
            <FontAwesomeIcon
              styleName="icon--color"
              icon={['far', 'arrow-alt-circle-up']}/>

            Withdraw
          </button>
        </div>
      </div>
    );
  }
}

export default BalanceInfo;
