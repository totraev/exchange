import React from 'react';
import {
  widget,
  ChartingLibraryWidgetOptions,
  IChartingLibraryWidget
} from '../../../../vendor/tradingView/charting_library.min';
import { options } from './config';


export type Props = React.HTMLProps<HTMLDivElement> & {
  options?: ChartingLibraryWidgetOptions
};

class TradingView extends React.PureComponent<Props> {
  static defaultProps: Props = { options };
  private tvWidget: IChartingLibraryWidget = null;

  componentDidMount(): void {
    this.tvWidget = new widget(this.props.options);
  }

  componentWillUnmount() {
    this.tvWidget.remove();
  }

  render() {
    return <div
      id={this.props.options.container_id}
      className={this.props.className}/>;
  }
}

export default TradingView;
