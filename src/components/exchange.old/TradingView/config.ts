import { ChartingLibraryWidgetOptions } from '../../../../vendor/tradingView/charting_library.min';
const { UDFCompatibleDatafeed } = require('../../../../vendor/tradingView/datafeeds.min');

export const options: ChartingLibraryWidgetOptions = {
  symbol: 'AAPL',
  interval: 'D',
  datafeed: new UDFCompatibleDatafeed('https://demo_feed.tradingview.com'),
  locale: 'en',
  container_id: 'tv_chart_container',
  library_path: '/tradingView/',
  charts_storage_url: 'https://saveload.tradingview.com',
  charts_storage_api_version: '1.1',
  client_id: 'tradingview.com',
  user_id: 'public_user_id',
  fullscreen: false,
  autosize: true,
  toolbar_bg: '#1f1f1f',
  enabled_features: [
    'hide_left_toolbar_by_default',
    'move_logo_to_main_pane',
    'keep_left_toolbar_visible_on_small_screens'
  ],
  disabled_features: [
    'use_localstorage_for_settings',
    'header_symbol_search',
    'header_settings',
    'main_series_scale_menu',
    'header_compare',
    'symbol_search_hot_key',
    'header_saveload',
    'control_bar',
    'timezone_menu',
    'go_to_date',
    'timeframes_toolbar',
    'edit_buttons_in_legend',
    'header_interval_dialog_button'
  ],
  custom_css_url: 'custom.css',
  loading_screen: { 
    backgroundColor: '#222222',
    foregroundColor: '#ffffff'
  },
  overrides: {
    'paneProperties.background': '#222222',
    'paneProperties.vertGridProperties.color': '#454545',
    'paneProperties.horzGridProperties.color': '#454545',
    'symbolWatermarkProperties.transparency': 90,
    'scalesProperties.textColor' : '#aaa',
    'symbolWatermarkProperties.color': 'rgba(0, 0, 0, 1)'
  },
  favorites: {
    intervals: ['D','2D', '3D', '1W', '3W', '1M', '6M'],
    chartTypes: []
  }
};
