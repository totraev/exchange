import React from 'react';
import './styles.sss';

export type Props = {
  append?: JSX.Element
};


export const BlockTitle: React.SFC<Props> = ({ children, append }) => (
  <h2 styleName="title">
    <span styleName="name">{children}</span>
    {append !== null
      ? <div styleName="append">
          {append}
        </div>
      : null}
  </h2>
);

BlockTitle.defaultProps = {
  append: null
};

export default BlockTitle;
