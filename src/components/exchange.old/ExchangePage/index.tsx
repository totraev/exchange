import React from 'react';
import { inject, observer } from 'mobx-react';
import { hot } from 'react-hot-loader';
import './styles.sss';

import { RouteComponentProps } from 'react-router-dom';
import { InjectStoreProps } from '../../../stores/RootStore';

import OrderForm from '../OrderForm';
import OrderBook from '../OrderBook';
import CandleStick from '../CandleStick';
import OrderTable from '../OrderTable';
import Header from '../Header';
import MarketInfo from '../MarketInfo';
import CurrPairToggle from '../CurrPairToggle';


export type RouteParams = {
  curr_pair: string
};

export type Props = RouteComponentProps<RouteParams> & InjectStoreProps;


@inject('rootStore') @observer
class ExchangePage extends React.Component<Props> {
  componentDidMount() {
    const { curr_pair } = this.props.match.params;

    this.props.rootStore.appStore.setCurrPair(curr_pair);
    this.props.rootStore.balanceStore.fetchUserBalances();
    this.props.rootStore.appStore.fetchCurrPairs();
    this.props.rootStore.l2UpdatesClient.connect();
    this.props.rootStore.l2UpdatesClient.on('open', () => {
      this.props.rootStore.l2UpdatesClient.subscribe(curr_pair);
    });
    this.props.rootStore.l3UserUpdatesClient.connect();
  }

  componentDidUpdate(prevProps) {
    const { curr_pair: prevCurrPair } = prevProps.match.params;
    const { curr_pair: nextCurrPair } = this.props.match.params;

    if (prevCurrPair !== nextCurrPair) {
      this.props.rootStore.appStore.setCurrPair(nextCurrPair);
      this.props.rootStore.l2UpdatesClient.subscribe(nextCurrPair);
      this.props.rootStore.appStore.loadStart();
    }
  }

  componentWillUnmount() {
    this.props.rootStore.l2UpdatesClient.disconnect();
    this.props.rootStore.l3UserUpdatesClient.disconnect();
  }

  render() {
    const { appStore } = this.props.rootStore;

    return <div styleName="wrap">
      <Header>
        <CurrPairToggle
          handleClick={cp => console.log(cp)}
          current={this.props.match.params.curr_pair}>
          {appStore.currPairs}
        </CurrPairToggle>

        <MarketInfo />
      </Header>
      
      
      <div styleName="container">
        <div styleName="section-1">
          <OrderForm />
          <OrderBook />
        </div>
        <div styleName="section-2">
          <CandleStick />
          <OrderTable />
        </div>
      </div>
    </div>;
  }
}

export default hot(module)(ExchangePage);
