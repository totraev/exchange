import React from 'react';
import { inject, observer } from 'mobx-react';
import './styles.sss';

import { InjectStoreProps } from '../../../stores/RootStore';

import BlockTitle from '../BlockTitle';
import BalanceInfo from '../BalanceInfo';


@inject('rootStore') @observer
class OrderForm extends React.Component<InjectStoreProps> {
  private tabStyleName(tab: 'limit' | 'market'): string {
    return this.props.rootStore.orderFormStore.type === tab
      ? 'tab--active'
      : 'tab';
  }

  private updateAmount: React.FormEventHandler<HTMLInputElement> = (e): void => {
    this.props.rootStore.orderFormStore.updateAmount(e.currentTarget.value);
  }

  private updatePrice: React.FormEventHandler<HTMLInputElement> = (e) => {
    this.props.rootStore.orderFormStore.updatePrice(e.currentTarget.value);
  }

  private handleSubmit: React.MouseEventHandler<HTMLButtonElement> = (e) => {
    e.preventDefault();
    this.props.rootStore.orderFormStore.createOrder();
  }

  private handleTabToggle(tab: 'limit' | 'market'): React.MouseEventHandler<HTMLAnchorElement> {
    const { orderFormStore } = this.props.rootStore;

    return (e) => {
      e.preventDefault();
      orderFormStore.setType(tab);
    };
  }

  private sideBtnStyleName(side: 'buy' | 'sell'): string {
    return this.props.rootStore.orderFormStore.side === side
      ? side
      : 'btn';
  }

  private handleSideToggle(side: 'buy' | 'sell'): React.MouseEventHandler<HTMLButtonElement> {
    const { orderFormStore } = this.props.rootStore;

    return (e) => {
      e.preventDefault();
      orderFormStore.setSide(side);
    };
  }

  render() {
    const { orderFormStore, appStore } = this.props.rootStore;
    const [amountCurr, priceCurr] = appStore.currentCurrencies;

    return <div styleName="wrap">
      <BlockTitle>Order Form</BlockTitle>

      <div styleName="container">
        <BalanceInfo />

        <form styleName="form">
          <ul styleName="nav">
            <li styleName={this.tabStyleName('market')}>
              <a href="#" onClick={this.handleTabToggle('market')}>MARKET</a>
            </li>
            <li styleName={this.tabStyleName('limit')}>
              <a href="#" onClick={this.handleTabToggle('limit')}>LIMIT</a>
            </li>
          </ul>

          <div styleName="buttons">
            <button
              styleName={this.sideBtnStyleName('buy')}
              onClick={this.handleSideToggle('buy')}>
              BUY
            </button>
            <button
              styleName={this.sideBtnStyleName('sell')}
              onClick={this.handleSideToggle('sell')}>
              SELL
            </button>
          </div>

          <div styleName="section">
            <label styleName="label" htmlFor="">Amount:</label>

            <div styleName="input-wrap">
              <input
                styleName="input"
                value={orderFormStore.amount}
                onChange={this.updateAmount}
                placeholder="0.00"/>

              <span styleName="curr-addon">{amountCurr}</span>
            </div>
          </div>

          {orderFormStore.type === 'limit' && (
            <div styleName="section">
              <label styleName="label" htmlFor="">Limit price:</label>

              <div styleName="input-wrap">
                <input
                  styleName="input"
                  value={orderFormStore.price}
                  onChange={this.updatePrice}
                  placeholder="0.00"/>

                <span styleName="curr-addon">{priceCurr}</span>
              </div>
            </div>
          )}

          <div styleName="total">
            <div styleName="currency">
              <b>Total</b> ({priceCurr}) ≈
            </div>
            <div styleName="value">{orderFormStore.total}</div>
          </div>

          <button
            onClick={this.handleSubmit}
            disabled={orderFormStore.isDisabled}
            styleName={`submit--${orderFormStore.side}`}>
            PLACE {orderFormStore.side.toUpperCase()} ORDER
          </button>
        </form>
      </div>
    </div>;
  }
}

export default OrderForm;
