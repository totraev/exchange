import React from 'react';
import { inject, observer } from 'mobx-react';
import { Route, Redirect, RouteProps, RouteComponentProps } from 'react-router-dom';

import { InjectStoreProps } from '../../../stores/RootStore';


export type Props = InjectStoreProps & RouteProps;

const PrivateRoute: React.SFC<Props> = ({ component: Component, rootStore, ...rest }) => {
  const render = (props: RouteComponentProps<any>): React.ReactNode => (
    rootStore.authStore.isAuth
      ? <Component {...props}/>
      : <Redirect to={{ pathname: '/auth/signin', state: { from: props.location } }}/>
  );

  return <Route {...rest} render={render}/>;
};

export default inject('rootStore')(observer(PrivateRoute));
