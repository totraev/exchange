import React from 'react';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import './styles.sss';

import { InjectStoreProps } from '../../../stores/RootStore';

export type Props = InjectStoreProps;


const Header: React.SFC<Props> = ({ rootStore: { authStore } }) => {
  return (
    <header styleName="header">
      <div styleName="brand">
        Logo
      </div>

      <nav styleName="nav">
        <Link styleName="nav-item" to="/">
          <FontAwesomeIcon styleName="icon--style" icon="home"/>
          <span>Main</span>
        </Link>
        <Link styleName="nav-item" to="/trade/BTCUSD">
          <FontAwesomeIcon styleName="icon--style" icon="sync-alt"/>
          <span>Exchange</span>
        </Link>
        <Link styleName="nav-item" to="/user/keys">
          <FontAwesomeIcon styleName="icon--style" icon="key"/>
          <span>Keys</span>
        </Link>
        <Link styleName="nav-item" to="/user/wallets">
          <FontAwesomeIcon styleName="icon--style" icon="wallet"/>
          <span>Wallets</span>
        </Link>
        <Link styleName="nav-item" to="/user/transactions">
          <FontAwesomeIcon styleName="icon--style" icon="exchange-alt"/>
          <span>Transactions</span>
        </Link>
      </nav>

      <div styleName="auth">
        {authStore.isAuth
          ? <button
            onClick={() => authStore.logout()}
            styleName="link-border">
              Logout
            </button>
          : <>
            <Link styleName="link" to="/auth/signin">Sign In</Link>
            <Link styleName="link-border" to="/auth/signup">Sign Up</Link>
          </>}
      </div>
    </header>
  );
};

export default inject('rootStore')(observer(Header));
