import React from 'react';
import './styles.sss';

export type Props = React.HTMLProps<HTMLInputElement> & {
  label?: string
  postfix?: string | JSX.Element
};

const Input: React.SFC<Props> = ({ label, postfix, ...props }) => {
  return (
    <div styleName="group">
      {label !== undefined && (
        <label styleName="label" htmlFor={props.id}>
          {label}
        </label>
      )}

      {postfix === undefined
        ? (
          <input
            styleName="field"
            {...props}/>
        )
        : (
          <div styleName="field-wrap">
            <input
              styleName="field field--padding"
              {...props}/>
            <span styleName="postfix">
              {postfix}
            </span>
          </div>
        )
      }
    </div>
  );
};

export default Input;
