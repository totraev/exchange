import React from 'react';
import './styles.sss';

export type Props = React.HTMLProps<HTMLButtonElement> & {
  color?: 'dark' | 'primary' | 'success' | 'danger'
};

const Button: React.SFC<Props> = ({ color, ...props }) => {
  return <button styleName={color} {...props}/>;
};

Button.defaultProps = {
  color: 'dark'
};

export default Button;
