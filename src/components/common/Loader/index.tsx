import React from 'react';
import './styles.sss';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';


export const Loader: React.SFC = () => <div styleName="loader">
  <FontAwesomeIcon styleName="loader--icon" icon="sync-alt" size="2x" spin/>
</div>;

export default Loader;
