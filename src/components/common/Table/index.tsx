import React from 'react';
import './styles.sss';

export type Props = React.HTMLProps<HTMLFontElement>;


const Table: React.SFC = ({ children, ...props }) => (
  <table styleName="table" {...props}>
    {children}
  </table>
);

export default Table;
