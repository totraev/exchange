import React from 'react';
import './styles.sss';

export type Props = {
  loading: boolean
};


export const Loader: React.SFC<Props> = ({ loading }) => (
  <div styleName={loading ? 'active' : 'loader'}>
    <div styleName="circle-1"/>
    <div styleName="circle-2"/>
    <div styleName="circle-3"/>
  </div>
);

export default Loader;
