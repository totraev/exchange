import React from 'react';
import './styles.sss';

export type Props = {
  row?: boolean
};

const FormGroup: React.SFC<Props> = ({ row, children }) => (
  <div styleName={row ? 'row' : 'column'}>
    {children}
  </div>
);

FormGroup.defaultProps = {
  row: false
};

export default FormGroup;
