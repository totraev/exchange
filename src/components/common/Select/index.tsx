import React from 'react';
import './styles.sss';

export type Props = React.HTMLProps<HTMLSelectElement> & {
  label?: string
};

const Select: React.SFC<Props> = ({ label, ...props }) => {
  return (
    <div styleName="group">
      {label !== undefined && (
        <label styleName="label" htmlFor={props.id}>
          {label}
        </label>
      )}

      <div styleName="select-wrap">
        <select styleName="select" {...props}/>
      </div>
    </div>
  );
};

export default Select;
