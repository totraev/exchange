import React from 'react';
import './styles.sss';

export type Props = {
  title: string
  append?: JSX.Element
  flex?: '1' | '2' | '3' | '4' | '5'
};

const Section: React.SFC<Props> = ({ flex, children, title, append }) => (
  <div styleName={`flex-${flex}`}>
    <h2 styleName="title">
      <span styleName="name">{title}</span>
      {append !== null
        ? <div styleName="append">
            {append}
          </div>
        : null}
    </h2>
    <div styleName="container">
      {children}
    </div>
  </div>
);

Section.defaultProps = {
  flex: '1'
};

export default Section;
