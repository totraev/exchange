import React from 'react';
import { inject, observer } from 'mobx-react';
import { Redirect } from 'react-router-dom';

import ProgressBar from '../ProgressBar';
import SignUpForm from '../SignUpForm';
import ConfirmForm from '../ConfirmForm';

import { InjectStoreProps } from '../../../stores/RootStore';


@inject('rootStore') @observer
class SignUpPage extends React.Component<InjectStoreProps> {
  componentWillUnmount() {
    this.props.rootStore.signUpStore.resetState();
  }

  private renderForm = (step: 1 | 2 | 3): JSX.Element => {
    switch (step) {
      case 1:
        return <SignUpForm />;
      case 2:
        return <ConfirmForm />;
      case 3:
        return <Redirect to="/auth/signin"/>;  
    }
  }

  render() {
    const { signUpStore } = this.props.rootStore;

    return <>
      <ProgressBar currentStep={signUpStore.step}/>

      {this.renderForm(signUpStore.step)}
    </>;
  }
}

export default SignUpPage;
