import React from 'react';
import { inject, observer } from 'mobx-react';
import { Redirect, RouteComponentProps } from 'react-router-dom';

import ProgressBar from '../ProgressBar';
// import ConfirmForm from '../ConfirmForm';
import SignInForm from '../SignInForm';

import { InjectStoreProps } from '../../../stores/RootStore';


export type Props = InjectStoreProps & RouteComponentProps<InjectStoreProps>;

@inject('rootStore') @observer
class SignInPage extends React.Component<Props> {
  componentWillUnmount() {
    this.props.rootStore.signInStore.resetState();
  }

  private renderForm = (step: 1 | 2): JSX.Element => {
    switch (step) {
      case 1:
        return <SignInForm />;
      case 2:
        const { from } = this.props.location.state || { from: { pathname: '/' } };
        return <Redirect push to={from}/>;
    }
  }

  render() {
    const { signInStore } = this.props.rootStore;

    return <>
      <ProgressBar currentStep={signInStore.step}/>

      {this.renderForm(signInStore.step)}
    </>;
  }
}

export default SignInPage;
