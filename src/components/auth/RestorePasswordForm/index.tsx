import React from 'react';
import './styles.sss';

import Form from '../Form';
import Button from '../Button';
import Input from '../Input';

export type Props = {
  onSubmit?: () => void
};

class RestorePasswordForm extends React.Component<Props> {
  static defaultProps = {
    onSubmit: () => {}
  };

  private handleSubmit = () => {
    this.props.onSubmit();
  }

  render() {
    return (
      <Form
        styleName="form"
        onSubmit={this.handleSubmit}
        title="Restore password"
        hint="Lorem ipsum dolor sit amet, euripidis interesset ne vim.
        Aliquam mentitum qui id.">

        <Input
          name="email"
          type="email"
          placeholder="Email"/>

        <Button type="submit">Continue</Button>
      </Form>
    );
  }
}

export default RestorePasswordForm;
