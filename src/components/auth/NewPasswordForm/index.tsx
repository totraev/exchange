import React from 'react';
import './styles.sss';

import Form from '../Form';
import Button from '../Button';
import Input from '../Input';

export type Props = {
  onSubmit?: () => void
};

class NewPasswordForm extends React.Component<Props> {
  static defaultProps = {
    onSubmit: () => {}
  };

  private handleSubmit = () => {
    this.props.onSubmit();
  }

  render() {
    return (
      <Form
        onSubmit={this.handleSubmit}
        styleName="form"
        title="New Password">

        <Input
          name="password"
          type="password"
          placeholder="Password"/>
        
        <Input
          name="password"
          type="password"
          placeholder="Confirm password"/>  

        <Button type="submit">Save</Button>
      </Form>
    );
  }
}

export default NewPasswordForm;
