import React from 'react';
import { inject, observer } from 'mobx-react';
// import { Link } from 'react-router-dom';
import './styles.sss';

import Button from '../Button';
import Input from '../Input';
import Form from '../Form';

import { InjectStoreProps } from '../../../stores/RootStore';


export type Props = InjectStoreProps;

@inject('rootStore') @observer
class SignInForm extends React.Component<Props> {
  private handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    this.props.rootStore.signInStore.handleSubmit();
  }

  private handleEmailChange = (e: React.FormEvent<HTMLInputElement>) => {
    this.props.rootStore.signInStore.updateField('email', e.currentTarget.value);
  }

  private handlePasswordChange = (e: React.FormEvent<HTMLInputElement>) => {
    this.props.rootStore.signInStore.updateField('password', e.currentTarget.value);
  }

  render() {
    const { signInStore } = this.props.rootStore;

    return (
      <Form
        onSubmit={this.handleSubmit}
        errors={signInStore.errors}
        styleName="login"
        title="Sign In"
        hint="Lorem ipsum dolor sit amet, euripidis interesset ne vim.
         Aliquam mentitum qui id.">

        <Input
          value={signInStore.fields.email}
          onChange={this.handleEmailChange}
          placeholder="Email"
          type="text"/> 

        <Input
          value={signInStore.fields.password}
          onChange={this.handlePasswordChange}
          placeholder="password"
          type="password"/>

        <Button
          type="submit"
          spinner={signInStore.loading}
          disabled={false}>
          Enter
        </Button>

        {/*<Link
          styleName="password"
          to="/auth/restore">
          Forgot password?
        </Link>*/}
      </Form>
    );
  }
}

export default SignInForm;
