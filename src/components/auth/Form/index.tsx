import React from 'react';
import './styles.sss';

export type Props = React.HTMLProps<HTMLFormElement> & {
  title: string
  hint?: string
  errors?: string[]
};


const Form: React.SFC<Props> = (props) => {
  const { title, hint, errors, className, children, ...formProps } = props;

  return (
    <div styleName="wrap" className={className}>
      <h2 styleName={Boolean(hint) ? 'title' : 'title--no-hint'}>{title}</h2>
      {Boolean(hint) && <p styleName="hint">{hint}</p>}

      {errors.length > 0 && (
        <ul styleName="errors">
          {errors.map((err, i) => <li key={i}>{err}</li>)}
        </ul>
      )}

      <form styleName="form" {...formProps}>
        {children}
      </form>
    </div>
  );
};

Form.defaultProps = {
  errors: []
};

export default Form;
