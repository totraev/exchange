import React from 'react';
import { inject, observer } from 'mobx-react';
import './styles.sss';

import Form from '../Form';
import Button from '../Button';
import Input from '../Input';

import { InjectStoreProps } from '../../../stores/RootStore';


export type Props = InjectStoreProps;

@inject('rootStore') @observer
class SignUpForm extends React.Component<Props> {
  private handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    this.props.rootStore.signUpStore.handleSubmit();
  }

  private handleEmailChange = (e: React.FormEvent<HTMLInputElement>) => {
    this.props.rootStore.signUpStore.updateField('email', e.currentTarget.value);
  }

  private handlePasswordChange = (e: React.FormEvent<HTMLInputElement>) => {
    this.props.rootStore.signUpStore.updateField('password', e.currentTarget.value);
  }

  private handlePasswordRepeatChange = (e: React.FormEvent<HTMLInputElement>) => {
    this.props.rootStore.signUpStore.updateField('repeatPassword', e.currentTarget.value);
  }

  render() {
    const { signUpStore } = this.props.rootStore;

    return (
      <Form
        onSubmit={this.handleSubmit}
        errors={signUpStore.errors}
        styleName="form"
        title="Sign Up"
        hint="Lorem ipsum dolor sit amet, euripidis interesset ne vim.
         Aliquam mentitum qui id">

        <Input
          name="email"
          value={signUpStore.fields.email}
          onChange={this.handleEmailChange}
          placeholder="Email"/>

        <Input
          type="password"
          name="password"
          value={signUpStore.fields.password}
          onChange={this.handlePasswordChange}
          placeholder="Password"/>

        <Input
          type="password"
          name="repeatPassword"
          value={signUpStore.fields.repeatPassword}
          onChange={this.handlePasswordRepeatChange}
          placeholder="Repeat password"/>  

        <Button
          type="submit"
          spinner={signUpStore.loading}>
          Continue
        </Button>
      </Form>
    );
  }
}


export default SignUpForm;
