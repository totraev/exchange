import React from 'react';
import './styles.sss';

export type Props = {
  type: string
};

const Step: React.SFC<Props> = ({ type }) => (
  <span styleName={type}/>
);

export default Step;

