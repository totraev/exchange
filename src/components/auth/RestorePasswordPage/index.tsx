import React from 'react';
import { inject, observer } from 'mobx-react';

import ProgressBar from '../ProgressBar';
import RestorePasswordForm from '../RestorePasswordForm';
import ConfirmForm from '../ConfirmForm';
import NewPasswordForm from '../NewPasswordForm';

import { InjectStoreProps } from '../../../stores/RootStore';

const RestorePasswordPage: React.SFC<InjectStoreProps> = (props) => {
  const { restorePasswordStore } = props.rootStore;

  const renderForm = (step: 1 | 2 | 3): JSX.Element => {
    switch (step) {
      case 1:
        return <RestorePasswordForm onSubmit={restorePasswordStore.incrStep}/>;
      case 2:
        return <ConfirmForm onSubmit={restorePasswordStore.incrStep}/>;
      case 3:
        return <NewPasswordForm onSubmit={restorePasswordStore.incrStep}/>;  
    }
  };

  return <>
    <ProgressBar currentStep={restorePasswordStore.step}/>

    {renderForm(restorePasswordStore.step)}
  </>;
};

export default inject('rootStore')(observer(RestorePasswordPage));
