import React from 'react';
import { RouteComponentProps, Route, Switch } from 'react-router-dom';
import { hot } from 'react-hot-loader';
import './styles.sss';

import SignInPage from '../SignInPage';
import SignUpPage from '../SignUpPage';
import RestorePasswordPage from '../RestorePasswordPage';

import Header from '../../common/Header';

class AuthPage extends React.Component<RouteComponentProps<{}>> {
  render() {
    const { match } = this.props;

    return <>
      <Header />
      <div styleName="wrap">
        <Switch>
          <Route path={`${match.path}/signup`} component={SignUpPage}/>
          <Route exact path={`${match.path}/signin`} component={SignInPage}/>
          <Route path={`${match.path}/restore`} component={RestorePasswordPage}/>
        </Switch>
      </div>
    </>;
  }
}

export default hot(module)(AuthPage);
