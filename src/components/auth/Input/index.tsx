import React from 'react';
import './styles.sss';

export type Props = React.HTMLProps<HTMLInputElement> & {
  invalid?: boolean
};


export class Input extends React.Component<Props> {
  public inputElement: HTMLInputElement;

  public render(): JSX.Element {
    const { invalid, ...inputProps } = this.props;

    return (
      <input
        styleName={invalid ? 'invalid' : 'input--default'}
        ref={input => this.inputElement = input}
        {...inputProps} />
    );
  }
}

export default Input;
