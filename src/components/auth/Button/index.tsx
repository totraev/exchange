import React from 'react';
import './styles.sss';

import Loader from '../../common/Loader';

export type Props = React.HTMLProps<HTMLButtonElement> & {
  spinner?: boolean
};


const Button: React.SFC<Props> = (props) => {
  const { spinner, disabled, children, ...btnProps } = props;

  return (
    <button
      styleName={spinner ? `button--loaded` : 'button'}
      disabled={spinner || disabled}
      {...btnProps}>
      {spinner ? <Loader /> : children}
    </button>
  );
};

export default Button;
