import React from 'react';
import './styles.sss';

import Form from '../Form';
import Button from '../Button';
import Input from '../Input';

export type Props = {
  onSubmit?: () => void
};

class ConfirmForm extends React.Component<Props> {
  static defaultProps = {
    onSubmit: () => {}
  };

  private handleSubmit = () => {
    this.props.onSubmit();
  }

  render() {
    return (
      <Form
        onSubmit={this.handleSubmit}
        styleName="form"
        title="Verify email"
        hint="Lorem ipsum dolor sit amet, euripidis interesset ne vim. Aliquam mentitum qui id.">

        <Input
          name="verificationCode"
          type="text"
          placeholder="verification code"/>

        <Button type="submit">Reset</Button>
      </Form>
    );
  }
}

export default ConfirmForm;
