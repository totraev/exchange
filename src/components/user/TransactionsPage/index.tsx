import React from 'react';
import { inject, observer } from 'mobx-react';
import FontAwesome from '@fortawesome/react-fontawesome';
import './styles.sss';

import Section from '../../common/Section';
import Table from '../../common/Table';
import Select from '../../common/Select';
import Input from '../../common/Input';
import FormGroup from '../../common/FormGroup';
import Button from '../../common/Button';
import Loader from '../../common/Loader';
import EmptyList from '../EmptyList';

import { InjectStoreProps } from '../../../stores/RootStore';

export type Props = InjectStoreProps;

@inject('rootStore') @observer
class TransactionsPage extends React.Component<Props> {
  componentDidMount() {
    this.props.rootStore.transactionsStore.loadWallets();
    this.props.rootStore.transactionsStore.loadTransactions();
  }

  private handeleChangeSelect = (e: React.FormEvent<HTMLSelectElement>) => {
    this.props.rootStore.transactionsStore.updateField('curr', e.currentTarget.value);
  }

  private handleChangeInput = (e: React.FocusEvent<HTMLInputElement>) => {
    this.props.rootStore.transactionsStore.updateField('amount', e.currentTarget.value);
  }

  private handleClick = () => {
    this.props.rootStore.transactionsStore.withdraw();
  }

  render() {
    const { transactionsStore } = this.props.rootStore;

    return (
      <div styleName="wrap">
        <Section flex="3" title="Transactions">
          {transactionsStore.loading 
            ? <Loader/>
            : transactionsStore.transactions.length > 0 ? (
              <Table>
                <thead>
                  <tr>
                    <th><FontAwesome icon="wallet"/> Currency</th>
                    <th><FontAwesome icon="exchange-alt"/> Type</th>
                    <th><FontAwesome icon="money-bill-alt"/> Amount</th>
                    <th><FontAwesome icon="check"/> Status</th>
                  </tr>
                </thead>
                <tbody>
                  {transactionsStore.transactions.map(transaction => (
                    <tr key={transaction.trans_id}>
                      <td>{transaction.curr}</td>
                      <td>{transaction.trans_type}</td>
                      <td><b>{transaction.amount}</b></td>
                      <td>{transaction.done
                        ? <FontAwesome icon="check-circle"/>
                        : transaction.err_desc}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            ) : (
              <EmptyList name="transactions"/>
          )}
        </Section>

        <Section flex="2" title="Withdrawal">
          <FormGroup>
            <Select
              value={transactionsStore.fields.curr}
              onChange={this.handeleChangeSelect}
              label="Wallet">
              <option></option>
              {transactionsStore.wallets
                .filter(({ address }) => address !== 'N/A')
                .map(({ curr }) => 
                  <option key={curr} value={curr}>{curr}</option>)}
            </Select>

            <Input
              value={transactionsStore.fields.amount}
              onChange={this.handleChangeInput}
              label="Amount"
              placeholder="0.01"/>
          </FormGroup>

          <Button
            disabled={!transactionsStore.isValid}
            onClick={this.handleClick}
            color="primary">
            WITHDRAW
          </Button>
        </Section>
      </div>
    );
  }
}

export default TransactionsPage;
