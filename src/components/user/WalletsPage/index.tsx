import React from 'react';
import { inject, observer } from 'mobx-react';
import FontAwesome from '@fortawesome/react-fontawesome';
import './styles.sss';

import Section from '../../common/Section';
import Table from '../../common/Table';
import Select from '../../common/Select';
import FormGroup from '../../common/FormGroup';
import Button from '../../common/Button';
import Loader from '../../common/Loader';
import EmptyList from '../EmptyList';

import { InjectStoreProps } from '../../../stores/RootStore';

export type Props = InjectStoreProps;


@inject('rootStore') @observer
class WalletsPage extends React.Component<Props> {
  componentDidMount() {
    this.props.rootStore.walletsStore.loadWallets();
  }

  private handleChange = (e: React.FormEvent<HTMLSelectElement>) => {
    this.props.rootStore.walletsStore.updateField('curr', e.currentTarget.value);
  }

  private handleClick = () => {
    this.props.rootStore.walletsStore.createWallet();
  }

  render() {
    const { walletsStore } = this.props.rootStore;

    return (
      <div styleName="wrap">
        <Section flex="3" title="Wallets">
          {walletsStore.loading 
            ? <Loader/> 
            : walletsStore.wallets.length > 0 ? (
              <Table>
                <thead>
                  <tr>
                    <th><FontAwesome icon="wallet"/> Name</th>
                    <th><FontAwesome icon="address-card"/> Address</th>
                  </tr>
                </thead>
                <tbody>
                  {walletsStore.wallets.map(({ curr, address }) => (
                    <tr key={curr}>
                      <td>{curr}</td>
                      <td>
                        <b>{address}</b>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            ) : (
              <EmptyList name="wallets"/>
          )}
        </Section>

        <Section flex="2" title="New Wallet">
          <FormGroup>
            <Select
              value={walletsStore.fields.curr}
              onChange={this.handleChange}
              label="Currency">
              <option></option>
              {walletsStore.wallets
                .filter(({ address }) => address === 'N/A')
                .map(({ curr }) => 
                  <option key={curr} value={curr}>{curr}</option>)}
            </Select>
          </FormGroup>

          <Button
            disabled={!walletsStore.isValid}
            onClick={this.handleClick}
            color="primary">
            Create wallet
          </Button>
        </Section>
      </div>
    );
  }
}

export default WalletsPage;
