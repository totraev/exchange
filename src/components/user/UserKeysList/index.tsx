import React from 'react';
import { observer, inject } from 'mobx-react';
import FontAwesome from '@fortawesome/react-fontawesome';
import './styles.sss';

import Loader from '../../common/Loader';
import EmptyList from '../EmptyList';

import { InjectStoreProps } from '../../../stores/RootStore';


const UserKeysList: React.SFC<InjectStoreProps> = ({ rootStore: { keysStore } }) => {
  const keyLevel = (value: 1 | 2 | 3) => {
    switch (value) {
      case 1:
        return 'View';
      case 2:
        return 'View, Trade';
      case 3:
        return 'View, Trade, Withdraw';    
    }
  };

  return keysStore.loading ? <Loader/> : keysStore.list.length > 0
    ? (
      <table styleName="table">
        <thead>
          <tr>
            <th>
              <FontAwesome styleName="icon--head" icon="key"/>
              Key
            </th>
            <th>
              <FontAwesome styleName="icon--head" icon="tags"/>
              Permissions
            </th>
            <th></th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          {keysStore.list.map(key => <React.Fragment key={key.id}>
            <tr key={key.id}>
              <td styleName="key">{key.id}</td>
              <td>{keyLevel(key.level)}</td>
              <td>
                <button
                  onClick={() => keysStore.toggleSecret(key.id)}
                  styleName="show">
                  {key.showSecret ? 'Hide secret' : 'Show secret'}
                </button>
              </td>
              <td>
                <button
                  onClick={() => keysStore.revokeKey(key.id)}
                  styleName="delete"
                  title="Delete">
                  <FontAwesome icon="trash"/>
                </button>
              </td>
            </tr>
            {key.showSecret 
              ? (
                <tr styleName="secret">
                  <td colSpan={4}>
                    <FontAwesome styleName="icon--head" icon="lock"/> Secret:
                    &nbsp;
                    {key.secret}
                  </td>
                </tr>
              )
              : false}
          </React.Fragment>)}
        </tbody>
      </table>
    )
  : (
    <EmptyList name="keys"/>
  ); 
};

export default inject('rootStore')(observer(UserKeysList));
