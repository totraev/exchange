import React from 'react';
import { observer, inject } from 'mobx-react';
import './styles.sss';

import Section from '../../common/Section';
import UserKeysList from '../UserKeysList';
import Select from '../../common/Select';
import FormGroup from '../../common/FormGroup';
import Button from '../../common/Button';


import { InjectStoreProps } from '../../../stores/RootStore';

@inject('rootStore') @observer
class ApiKeysPage extends React.Component<InjectStoreProps> {
  componentDidMount() {
    this.props.rootStore.keysStore.getKeys();
  }

  private handeSelect = (e: React.FormEvent<HTMLSelectElement>) => {
    const { value } = e.currentTarget;
    const level = parseInt(value, 10);
    this.props.rootStore.keysStore.toggleKeyLevel(level as 1 | 2 | 3);
  }

  render() {
    const { keysStore } = this.props.rootStore;

    return (
      <div styleName="wrap">
        <Section flex="3" title="Api keys">
          <UserKeysList />
        </Section>

        <Section flex="2" title="Create new key">
          <FormGroup row>
            <Select
              value={keysStore.keyLevel}
              onChange={this.handeSelect}
              label="Key level">
              <option value="1">View</option>
              <option value="2">View, Trade</option>
              <option value="3">View, Trade, Withdraw</option>
            </Select>
          </FormGroup>

          <Button
            onClick={() => keysStore.createKey()}
            disabled={keysStore.btnDisabled}
            color="primary">
            Create Api Key
          </Button>
        </Section>
      </div>
    );
  }
}

export default ApiKeysPage;
