import React from 'react';
import './styles.sss';

export type Props = {
  name: string
};

const EmptyList: React.SFC<Props> = ({ name }) => (
  <div styleName="empty">
    You have no any {name}
  </div>
);

export default EmptyList;
