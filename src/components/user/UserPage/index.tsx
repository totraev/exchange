import React from 'react';
import { RouteComponentProps, Route, Switch } from 'react-router-dom';
import { hot } from 'react-hot-loader';
import './styles.sss';

import Header from '../../common/Header';
import ApiKeysPage from '../ApiKeysPage';
import WalletsPage from '../WalletsPage';
import TransactionsPage from '../TransactionsPage';


class UserPage extends React.Component<RouteComponentProps<{}>> {
  render() {
    const { match } = this.props;

    return <div styleName="wrap">
      <Header />
      <Switch>
        <Route path={`${match.path}/keys`} component={ApiKeysPage}/>
        <Route path={`${match.path}/wallets`} component={WalletsPage}/>
        <Route path={`${match.path}/transactions`} component={TransactionsPage}/>
      </Switch>
    </div>;
  }
}

export default hot(module)(UserPage);
