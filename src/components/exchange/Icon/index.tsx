import React, { SFC, HTMLProps } from 'react';
import './styles.sss';

export interface IProps extends HTMLProps<HTMLElement> {
  name?: 'search' | 'star-fill' | 'star-empty';
}

const Icon: SFC<IProps> = ({ name, className }) => (
  <i styleName={name} className={className}/>
);


export default Icon;
