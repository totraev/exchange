import React, { Component } from 'react';
import './styles.sss';

import { Link } from 'react-router-dom';


class Footer extends Component {
  public render() {
    return (
      <footer styleName="footer">
        <div styleName="error">
          12:34:15 Ордер не может быть отправлен: недостаточно баланса марджию
        </div>

        <nav styleName="nav">
          <Link styleName="nav-item" to="#">О бирже</Link>
          <Link styleName="nav-item" to="#">Документация</Link>
          <Link styleName="nav-item" to="#">API</Link>
        </nav>

        <button styleName="dark"/>
      </footer>
    );
  }
}

export default Footer;
