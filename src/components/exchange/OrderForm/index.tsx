import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './styles.sss';

import CustomScrollbar from '@components/exchange/CustomScrollbar';

class OrderForm extends Component {
  public render() {
    return (
      <div styleName="order-form">
        <div styleName="head">
          <h2 styleName="title">Order</h2>

          <Link to="#" styleName="about">О контракте BTCBCH</Link>
        </div>

        <div styleName="tabs">
          <button styleName="tab--active">Рыночный</button>
          <button styleName="tab">Лимитный</button>
          <button styleName="tab">С остановкой</button>
        </div>
        
        <div styleName="container">
          <CustomScrollbar>
            {/* Market order */}
            {true && <div styleName="market">
              <div styleName="field-group">
                <label styleName="field-label" htmlFor="">Volume</label>
                <div styleName="field-wrap">
                  <input styleName="field-input" type="text"/>
                  <span styleName="field-append">BTC</span>
                </div>
              </div>

              <div styleName="field-group">
                <label styleName="field-label" htmlFor="">Market price</label>
                <div styleName="field-wrap">
                  <input styleName="field-input" type="text"/>
                  <span styleName="field-append">BTC</span>
                </div>
              </div>

              <div styleName="switcher-group">
                <label styleName="field-label" htmlFor="">Кредитное плечо</label>
                <div styleName="field-wrap">
                  Switcher
                </div>
              </div>

              <div styleName="margin">
                <div styleName="margin-labels">
                  Маржа сделки
                  <br/>
                  Доступно маржи
                </div>

                <div styleName="margin-values">
                  4.19425250 BTC
                  <br/>
                  432.19425250 BTC
                </div>
              </div>

              <div styleName="buttons">
                <button styleName="btn--buy">Buy</button>
                <button styleName="btn--sell">Sell</button>
              </div>
            </div>}

            {/* Limit order */}
            {false && <div styleName="limit">
              <div styleName="field-group">
                <label styleName="field-label" htmlFor="">Volume</label>
                <div styleName="field-wrap">
                  <input styleName="field-input" type="text"/>
                  <span styleName="field-append">BTC</span>
                </div>
              </div>

              <div styleName="field-group">
                <label styleName="field-label" htmlFor="">Market price</label>
                <div styleName="field-wrap">
                  <input styleName="field-input" type="text"/>
                  <span styleName="field-append">BTC</span>
                </div>
              </div>

              <div styleName="switcher-group">
                <label styleName="field-label" htmlFor="">Кредитное плечо</label>
                <div styleName="field-wrap">
                  Switcher
                </div>
              </div>

              <div styleName="margin">
                <div styleName="margin-labels">
                  Маржа сделки
                  <br/>
                  Доступно маржи
                </div>

                <div styleName="margin-values">
                  4.19425250 BTC
                  <br/>
                  432.19425250 BTC
                </div>
              </div>

              <div styleName="buttons">
                <button styleName="btn--buy">Buy</button>
                <button styleName="btn--sell">Sell</button>
              </div>
            </div>}
            
            {/* Stop order */}
            {false && <div styleName="stop">
              <div styleName="field-group">
                <label styleName="field-label" htmlFor="">Volume</label>
                <div styleName="field-wrap">
                  <input styleName="field-input" type="text"/>
                  <span styleName="field-append">BTC</span>
                </div>
              </div>

              <div styleName="field-group">
                <label styleName="field-label" htmlFor="">Market price</label>
                <div styleName="field-wrap">
                  <input styleName="field-input" type="text"/>
                  <span styleName="field-append">BTC</span>
                </div>
              </div>

              <div styleName="switcher-group">
                <label styleName="field-label" htmlFor="">Кредитное плечо</label>
                <div styleName="field-wrap">
                  Switcher
                </div>
              </div>

              <div styleName="margin">
                <div styleName="margin-labels">
                  Маржа сделки
                  <br/>
                  Доступно маржи
                </div>

                <div styleName="margin-values">
                  4.19425250 BTC
                  <br/>
                  432.19425250 BTC
                </div>
              </div>

              <div styleName="buttons">
                <button styleName="btn--buy">Buy</button>
                <button styleName="btn--sell">Sell</button>
              </div>
            </div>}
          </CustomScrollbar>
        </div>
      </div>
    );
  }
}

export default OrderForm;
