import React, { SFC } from 'react';
import './styles.sss';

const OrderPositionTable: SFC = () => {
  return (
    <table styleName="table">
      <thead>
        <tr>
          <th>Символ</th>
          <th styleName="h-desc">Описание</th>
          <th styleName="text-right">Объем</th>
          <th styleName="text-right">Сумма позиции</th>
          <th styleName="text-right">Цена входа</th>
          <th styleName="text-right">Цена маркировки</th>
          <th styleName="text-right">Цена ликвидации</th>
          <th styleName="text-right">Маржа и плечо</th>
          <th></th>
          <th>Profit/Loss</th>
          <th>Время открытия</th>
          <th>
            <button styleName="close-all">Закрыть все</button>
          </th>
        </tr>
      </thead>

      <tbody>
        {Array(10).fill(0).map((_, i) => (
          <tr key={i}>
            <td styleName="symbol">BTCBCH</td>
            <td styleName="desc">Bitcoin Cash в котируемую Bitcoin валюту</td>
            <td styleName="text-right">
              <span styleName="plus">+</span>
              5.000
            </td>
            <td styleName="curr text-right">
              65.01633829
              &nbsp;
              <span>BCH</span>
            </td>
            <td styleName="text-right">8400.05</td>
            <td styleName="text-right">8400.05</td>
            <td styleName="text-right">8400.15</td>
            <td styleName="text-right">12.03045513</td>
            <td>
              <select styleName="leverage" name="" id="">
                <option>10x</option>
              </select>
            </td>
            <td styleName="stats--down">
              <img src={require('./svg/mini-chart.svg')} alt=""/>
              &nbsp;
              0.0005
              &nbsp;
              <span>BTC</span>
            </td>
            <td>21 мая, 10:30:21</td>
            <td></td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default OrderPositionTable;
