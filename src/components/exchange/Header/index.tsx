import React, { SFC } from 'react';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import { translate, InjectedTranslateProps } from 'react-i18next';
import './styles.sss';

import { InjectedExchangeStore } from '@stores/exchange/exchangeStore';


export type Props = InjectedTranslateProps & InjectedExchangeStore;

const Header: SFC<Props> = ({ t, exchangeStore }) => {
  return (
    <div styleName="header">
      <Link styleName="brand" to="/trade/BTCUSD">
        <span styleName="logo"/>
      </Link>

      <div styleName="info">
        <div styleName="balance">
          <div styleName="labels">
            <span styleName="label">{t('header.balance')}</span>
            <span styleName="label">{t('header.available')}</span>
          </div>

          <div styleName="values">
            <span styleName="value">
              5,567.54
              <span styleName="curr"> BTC</span>
            </span>

            <span styleName="value">
              3,540.21
              <span styleName="curr"> BTC</span>
            </span>
          </div>
        </div>

        <div styleName="balance">
          <div styleName="labels">
            <span styleName="label">{t('header.margin')}</span>
            <span styleName="label">{t('header.used')}</span>
          </div>

          <div styleName="values">
            <span styleName="value">
              5,567.54
              <span styleName="curr"> BTC</span>
            </span>

            <span styleName="value">
              3,540.21
              <span styleName="curr"> BTC</span>
            </span>
          </div>
        </div>

        <div styleName="pnl">
          <div styleName="pnl-label">{t('header.pnl')}</div>
          <div styleName="pnl-value">
            <span styleName="mini-chart">
              <svg xmlns="http://www.w3.org/2000/svg" width="65" height="15" viewBox="0 0 65 15">
                <g fill="none" fillRule="evenodd" transform="translate(-1)">
                  <path
                    stroke="#3B6956"
                    strokeWidth=".5"
                    d="M1 9.4l3.489-2.8 3.488 4.2h5.233L15.826 1l6.978 1.4L26.292 8l3.489-2.1
                      4.36 7.7L37.63 8l4.36-1.4 1.745 7 4.36-5.6 2.617 7 4.36-5.6L62.05 9V8"/>
                  <ellipse cx="63.143" cy="9.002" fill="#3B6956" rx="2" ry="2.002"/>
                </g>
              </svg>
            </span>
            <span>567.54</span>
            <span styleName="pnl-curr">BTC</span>
          </div>
        </div>

        <div styleName="menu"/>
      </div>

      <button styleName="toggle" onClick={exchangeStore.toggleSidebar}>
        <span styleName="stripe"/>
        <span styleName="stripe"/>
        <span styleName="stripe"/>
      </button>
    </div>
  );
};

export default translate('exchange')(inject('exchangeStore')(observer(Header)));
