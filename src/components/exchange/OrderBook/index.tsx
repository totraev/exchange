import React, { PureComponent } from 'react';
import './styles.sss';

import CustomScrollbar from '@components/exchange/CustomScrollbar';

class OrderBook extends PureComponent {
  private volumeIntecator(side: 'buy' | 'sell', total: number, volume: number) {
    const val = Math.ceil((volume / total) * 100);
    const color = side === 'buy' ? 'rgba(59, 105, 86, 0.25)' : 'rgba(255, 79, 84, 0.15)';

    return {
      backgroundImage: `linear-gradient(to right, ${color} ${val}%, transparent ${val}%)`
    };
  }

  public render() {
    return (
      <div styleName="order-book">
        <div styleName="head">
          <div styleName="title">Объём BCH</div>
          <div styleName="title">Цена BTC</div>
          <div styleName="title">Мой объём</div>
        </div>

        <div styleName="container">
          <CustomScrollbar>
            <table styleName="sell">
              <tbody>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 17)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 50)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 17)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 50)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 17)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 50)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 17)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 50)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 17)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 50)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 17)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 50)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 17)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 50)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 17)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 50)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 17)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 50)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 17)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('sell', 100, 50)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
              </tbody>
            </table>

            <div styleName="spread">
              <div styleName="label">Спред</div>
              <div styleName="value"><span>000</span>1.20</div>
            </div>

            <table styleName="buy">
              <tbody>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 32)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 3)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 15)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 32)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 3)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 15)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 32)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 3)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 15)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 32)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 3)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 15)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 32)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 3)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 15)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 32)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 3)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 15)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 32)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 3)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 15)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 32)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 3)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 15)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 32)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 3)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 15)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 32)}>
                  <td styleName="volume">8.02633829</td>
                  <td styleName="price">8508.56</td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 3)}>
                  <td styleName="volume">0.11<span>000000</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
                <tr styleName="price-lvl" style={this.volumeIntecator('buy', 100, 15)}>
                  <td styleName="volume">120.006417<span>00</span></td>
                  <td styleName="price">8508.<span>56</span></td>
                  <td styleName="my-volume">5.11<span>000000</span></td>
                </tr>
              </tbody>
            </table>
          </CustomScrollbar>
        </div>
      </div>
    );
  }
}

export default OrderBook;
