import React, { SFC, KeyboardEvent, FormEvent } from 'react';
import { inject, observer } from 'mobx-react';
import { translate, InjectedTranslateProps } from 'react-i18next';
import './styles.sss';

import { key } from './constants';

import { InjectedExchangeStore } from '@stores/exchange/exchangeStore';
import CustomScrollbar from '@components/exchange/CustomScrollbar';
import Icon from '@components/exchange/Icon';

export interface IProps extends InjectedTranslateProps, InjectedExchangeStore {}

const CurrencySelector: SFC<IProps> = ({ t, exchangeStore: { chartStore } }) => {
  const handleRowClick = (index: number) => () => chartStore.selectPair(index);

  const handleKeyDown = (e: KeyboardEvent<HTMLInputElement>) => {
    switch (e.keyCode) {
      case key.ARROW_UP:
        e.preventDefault();
        return chartStore.handlePrevHovered();
      case key.ARROW_DOWN:
        e.preventDefault();
        return chartStore.handleNextHovered();
      case key.ENTER:
        e.preventDefault();
        return chartStore.selectPair(chartStore.search.hovered);
    }
  };

  const handleFilter = (e: FormEvent<HTMLInputElement>) => 
    chartStore.handleChange(e.currentTarget.value);

  return <>
    <div
      styleName={chartStore.search.active ? 'head--active' : 'head'}
      onClick={chartStore.activateSearch}>
      <Icon styleName="icon" name="search" />

      <div styleName="curr">{chartStore.selectedCurr.name} {chartStore.selectedCurr.price}</div>

      <ul styleName="stats">
        <li>
          {t('trading_view.titles.change')}
          &ensp;
          <span styleName="down">{chartStore.selectedCurr.change}</span>
        </li>
        <li>{t('trading_view.titles.high')} 8407.65</li>
        <li>{t('trading_view.titles.low')} 8402.65</li>
      </ul>

      {chartStore.search.active && <div styleName="search">
        <input
          autoFocus={true}
          value={chartStore.search.value}
          onChange={handleFilter}
          onKeyDown={handleKeyDown}
          styleName="input"
          type="text" />
      </div>}
    </div>

    <div
      styleName={chartStore.search.active ? 'search-results--active' : 'search-results'}
      onClick={chartStore.deactivateSearch}>
      <CustomScrollbar>
        <table>
          <thead>
            <tr>
              <th></th>
              <th>{t('trading_view.titles.symbol')}</th>
              <th styleName="desc-title">{t('trading_view.titles.desc')}</th>
              <th></th>
              <th>{t('trading_view.titles.price')}</th>
              <th>{t('trading_view.titles.change')}</th>
            </tr>
          </thead>

          <tbody>
            {chartStore.filteredCurrPairs.map((currPair, idx) => (
              <tr
                key={currPair.name}
                onClick={handleRowClick(idx)}
                styleName={chartStore.search.hovered === idx ? 'curr-row--hovered' : 'curr-row'}
              >
                <td>
                  <button styleName="favorite">
                    {currPair.favorite
                      ? <Icon name="star-fill"/>
                      : <Icon name="star-empty"/>}
                  </button>
                </td>
                <td styleName="symbol">{currPair.name}</td>
                <td styleName="desc">{t(`trading_view.descriptions.${currPair.name}`)}</td>
                <td>
                  <img src={require('./svg/mini-chart.svg')}/>
                </td>
                <td styleName="curr-price">
                  {currPair.price}
                </td>
                <td styleName="change--down">{currPair.change}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </CustomScrollbar>
    </div>
  </>;
};

export default translate('exchange')(inject('exchangeStore')(observer(CurrencySelector)));
