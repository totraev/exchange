import React, { Component } from 'react';

import Scrollbars from 'react-custom-scrollbars';

class CustomScrollbar extends Component {
  private renderThumbVertical({ style }) {
    const styles = {
      width: '6px',
      borderRadius: '3px',
      backgroundColor: 'rgba(255, 255, 255, 0.1)',
      border: 'solid 0.1px rgba(221, 221, 221, 0.4)'
    };

    return <div style={{ ...style, ...styles }}/>;
  }

  private renderTrackVertical({ style }) {
    const styles = {
      right: '4px',
      top: '5px',
      bottom: '5px'
    };

    return <div style={{ ...style, ...styles }}/>;
  }

  public render() {
    const { children } = this.props;

    return (
      <Scrollbars
        autoHide={false}
        renderThumbVertical={this.renderThumbVertical}
        renderTrackVertical={this.renderTrackVertical}
        style={{ width: '100%', height: '100%' }}
      >
        {children}
      </Scrollbars>
    );
  }
}

export default CustomScrollbar;
