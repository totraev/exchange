import React, { Component } from 'react';
import './styles.sss';

import CustomScrollbar from '@components/exchange/CustomScrollbar';
import OrderPositionTable from '@components/exchange/OrderPositionTable';

class OrderTable extends Component {
  public render() {
    return (
      <div styleName="order-table">
        <div styleName="tabs">
          <CustomScrollbar>
            <div styleName="tabs-wrap">
              <button styleName="tab--active">Позиция</button>
              <button styleName="tab">Ордера</button>
              <button styleName="tab">Стопы</button>
              <button styleName="tab">Исполненные</button>
              <button styleName="tab">История</button>
            </div>
          </CustomScrollbar>
        </div>

        <div styleName="content">
          <CustomScrollbar>
            <OrderPositionTable />
          </CustomScrollbar>
        </div>
      </div>
    );
  }
}

export default OrderTable;
