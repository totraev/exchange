import { ChartingLibraryWidgetOptions } from '../../../../vendor/tradingView/charting_library.min';
const { UDFCompatibleDatafeed } = require('../../../../vendor/tradingView/datafeeds.min');

export const resolutions = ['D', '2D', '3D', 'W', '3W', 'M', '6M'];

export const options: ChartingLibraryWidgetOptions = {
  symbol: 'AAPL',
  interval: 'D',
  datafeed: new UDFCompatibleDatafeed('https://demo_feed.tradingview.com'),
  locale: 'en',
  container_id: 'tv_chart_container',
  library_path: '/tradingView/',
  charts_storage_url: 'https://saveload.tradingview.com',
  charts_storage_api_version: '1.1',
  client_id: 'tradingview.com',
  user_id: 'public_user_id',
  fullscreen: false,
  autosize: true,
  enabled_features: [
    'move_logo_to_main_pane',
    'keep_left_toolbar_visible_on_small_screens',
    'dont_show_boolean_study_arguments',
    'hide_last_na_study_output',
    'remove_library_container_border',
  ],
  disabled_features: [
    'use_localstorage_for_settings',
    'header_symbol_search',
    'header_settings',
    'main_series_scale_menu',
    'header_compare',
    'symbol_search_hot_key',
    'header_saveload',
    'control_bar',
    'timezone_menu',
    'go_to_date',
    'timeframes_toolbar',
    'edit_buttons_in_legend',
    'header_interval_dialog_button',
    'legend_context_menu',
    'scales_context_menu',
    'pane_context_menu',
    'border_around_the_chart',
    // test
    'edit_buttons_in_legend',
    'context_menus',
    'display_market_status',
    'chart_property_page_style',
    'property_pages',
    'show_chart_property_page',
    'chart_property_page_scales',
    'chart_property_page_background',
    'chart_property_page_timezone_sessions',
    'chart_property_page_trading',
    'countdown',
    'symbol_info',
    'source_selection_markers',
    'header_indicators',
    'header_resolutions'
  ],
  custom_css_url: 'custom.css',
  toolbar_bg: '#0c0c15',
  loading_screen: {
    backgroundColor: '#0c0c15',
    foregroundColor: '#ffffff'
  },
  overrides: {
    'paneProperties.background': '#0c0c15',
    'paneProperties.vertGridProperties.color': 'rgba(255, 255, 255, 0.2)',
    'paneProperties.horzGridProperties.color': 'transparent',
    'symbolWatermarkProperties.transparency': 90,
    'scalesProperties.textColor' : '#a0a1a4',
    'scalesProperties.lineColor': 'rgba(255, 255, 255, 0.2)',
    // Legends
    'paneProperties.legendProperties.showStudyArguments': false,
    'paneProperties.legendProperties.showStudyTitles': false,
    'paneProperties.legendProperties.showStudyValues': false,
    'paneProperties.legendProperties.showSeriesTitle': false,
    'paneProperties.legendProperties.showSeriesOHLC': false,
    // Candles
    'mainSeriesProperties.candleStyle.upColor': '#3b6956',
    'mainSeriesProperties.candleStyle.downColor': '#af363a',
    'mainSeriesProperties.candleStyle.borderColor': '#378658',
    'mainSeriesProperties.candleStyle.borderUpColor': '#3b6956',
    'mainSeriesProperties.candleStyle.borderDownColor': '#af363a',
    'mainSeriesProperties.candleStyle.wickUpColor': '#3b6956',
    'mainSeriesProperties.candleStyle.wickDownColor': '#af363a',
    'paneProperties.topMargin': 5,
    'paneProperties.bottomMargin': 15
  },
  studies_overrides: {
    'volume.volume.color.0': 'rgba(59, 105, 86, 0.25)',
    'volume.volume.color.1': 'rgba(175, 54, 58, 0.2)'
  }
};
