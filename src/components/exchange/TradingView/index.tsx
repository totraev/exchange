import React, { Component, HTMLProps } from 'react';
import { observer, inject } from 'mobx-react';
import './styles.sss';

import classNames from 'classnames';
import clickOutside from 'react-click-outside';
import { translate, InjectedTranslateProps } from 'react-i18next';
import {
  widget,
  ChartingLibraryWidgetOptions,
  IChartingLibraryWidget
} from '../../../../vendor/tradingView/charting_library.min';

import { InjectedExchangeStore } from '@stores/exchange/exchangeStore';
import CurrencySelect from '@components/exchange/CurrencySelector';

import { options, resolutions } from './config';

export interface Props extends
  HTMLProps<HTMLDivElement>,
  InjectedTranslateProps,
  InjectedExchangeStore {
  options?: ChartingLibraryWidgetOptions;
}

class TradingView extends Component<Props> {
  public static defaultProps: Partial<Props> = { options };
  private widget: IChartingLibraryWidget = null;
  private resolutionButtons: { [key: string]: JQuery<HTMLElement> } = {};

  public componentDidMount(): void {
    this.widget = new widget(this.props.options);
    this.widget.onChartReady(this.renderResolutionButtons);
  }

  private renderResolutionButtons = () => {
    const { t } = this.props;
    const resolution = this.widget.chart().resolution();

    resolutions.forEach((res) => {
      this.resolutionButtons[res] = this.widget.createButton()
        .attr('title', t(`trading_view.resolutions.${res}`))
        .on('click', this.handleResolutionChange(res))
        .prepend(`<span>${t(`trading_view.resolutions.${res}`)}</span>`)
        .parent()
        .addClass(classNames('resolution-button', {
          active: res === resolution
        }))
        .first();
    });
  }

  private handleResolutionChange = (res: string) => {
    return () => {
      this.widget.chart().setResolution(res, null);

      Object
        .values(this.resolutionButtons)
        .forEach($button => $button.removeClass('active'));

      this.resolutionButtons[res].addClass('active');
    };
  }

  // @ts-ignore: noUnusedLocals
  private handleClickOutside = () => {
    this.props.exchangeStore.chartStore.deactivateSearch();
  }

  public render() {
    return <div styleName="chart">
      <CurrencySelect />
  
      <div
        id={this.props.options.container_id}
        styleName="trading-view"/>
    </div>;
  }
}

export default translate('exchange')(inject('exchangeStore')(clickOutside(observer(TradingView))));

export * from './config';
