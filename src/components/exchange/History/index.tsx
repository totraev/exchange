import React, { Component } from 'react';
import './styles.sss';

import CustomScrollbar from '@components/exchange/CustomScrollbar';

class History extends Component {
  private volumeIntecator(side: 'buy' | 'sell', total: number, volume: number) {
    const val = Math.ceil((volume / total) * 100);
    const color = side === 'buy' ? 'rgba(59, 105, 86, 0.25)' : 'rgba(255, 79, 84, 0.15)';

    return {
      backgroundImage: `linear-gradient(to right, ${color} ${val}%, transparent ${val}%)`
    };
  }

  public render() {
    return (
      <div styleName="history">
        <div styleName="head">
          <div styleName="title">14:42:09</div>
          <div styleName="title">Последние сделки</div>
        </div>

        <div styleName="container">
          <CustomScrollbar>
            <table styleName="table">
              <tbody>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 57)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 33)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 15)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 57)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 11)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 25)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 57)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 33)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 15)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 57)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 11)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 25)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 57)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 33)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 15)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 57)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 11)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 25)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 57)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 33)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 15)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 5)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 57)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
                <tr styleName="sell" style={this.volumeIntecator('sell', 100, 11)}>
                  <td styleName="time">12:48:09</td>
                  <td styleName="price">8409.56</td>
                  <td styleName="volume">8.01633829</td>
                </tr>
                <tr styleName="buy" style={this.volumeIntecator('buy', 100, 25)}>
                  <td styleName="time">12:47:04</td>
                  <td styleName="price">8406.77</td>
                  <td styleName="volume">0.11<span>000000</span></td>
                </tr>
              </tbody>
            </table>
          </CustomScrollbar>
        </div>
      </div>
    );
  }
}

export default History;
