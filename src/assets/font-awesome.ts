import fontawesome from '@fortawesome/fontawesome';

import faArrowAltCircleDown from '@fortawesome/fontawesome-free-regular/faArrowAltCircleDown';
import faArrowAltCircleUp from '@fortawesome/fontawesome-free-regular/faArrowAltCircleUp';
import faAngleDown from '@fortawesome/fontawesome-free-solid/faAngleDown';
import faSyncAlt from '@fortawesome/fontawesome-free-solid/faSyncAlt';
import faKey from '@fortawesome/fontawesome-free-solid/faKey';
import faHome from '@fortawesome/fontawesome-free-solid/faHome';
import faTrash from '@fortawesome/fontawesome-free-solid/faTrash';
import faLock from '@fortawesome/fontawesome-free-solid/faLock';
import faTags from '@fortawesome/fontawesome-free-solid/faTags';
import faWallet from '@fortawesome/fontawesome-free-solid/faWallet';
import faAddresscard from '@fortawesome/fontawesome-free-solid/faAddressCard';
import faExchangeAlt from '@fortawesome/fontawesome-free-solid/faExchangeAlt';
import faMoneyBillAlt from '@fortawesome/fontawesome-free-solid/faMoneyBillAlt';
import faCheck from '@fortawesome/fontawesome-free-solid/faCheck';
import faCheckCircle from '@fortawesome/fontawesome-free-solid/faCheckCircle';

fontawesome.library.add(
  faArrowAltCircleUp,
  faArrowAltCircleDown,
  faAngleDown,
  faSyncAlt,
  faKey,
  faHome,
  faTrash,
  faLock,
  faTags,
  faWallet,
  faAddresscard,
  faExchangeAlt,
  faMoneyBillAlt,
  faCheck,
  faCheckCircle
);
