import React from 'react';
import ReactDOM from 'react-dom';
import { configure } from 'mobx';
import { Provider } from 'mobx-react';

import './assets/fonts/styles.css';
import './assets/fonts/stylesheet.css';
import './assets/css/main.sss';

import './assets/font-awesome';
import './i18n';

import App from '@views/common/App';
import RootStore from '@stores/RootStore';

import exchangeStore from '@stores/exchange/exchangeStore';

configure({
  enforceActions: true
});

const rootStore = new RootStore();

ReactDOM.render(
  <Provider
    rootStore={rootStore}
    exchangeStore={exchangeStore}>
    <App/>
  </Provider>,
  document.getElementById('app')
);
