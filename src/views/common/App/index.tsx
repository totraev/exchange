import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import { hot } from 'react-hot-loader';
import Loadable from 'react-loadable';
import './styles.sss';

import Loader from '@components/common/PageLoader';
import PrivateRoute from '@components/common/PrivateRoute';


const StaticPage = Loadable({
  loader: () => import(
    /* webpackChunkName: "static-page" */
    '@views/common/StaticPage'
  ),
  loading: () => <Loader loading/>
});

const ExchangePageOld = Loadable({
  loader: () => import(
    /* webpackChunkName: "exchange-page-old" */
    '@components/exchange.old/ExchangePage'
  ),
  loading: () => <Loader loading/>
});

const ExchangePage = Loadable({
  loader: () => import(
    /* webpackChunkName: "exchange-page" */
    '@views/exchange/ExchangePage'
  ),
  loading: () => <Loader loading/>
});

const AuthPage = Loadable({
  loader: () => import(
    /* webpackChunkName: "auth-page" */
    '@components/auth/AuthPage'
  ),
  loading: () => <Loader loading/>
});

const UserPage = Loadable({
  loader: () => import(
    /* webpackChunkName: "user-page" */
    '@components/user/UserPage'
  ),
  loading: () => <Loader loading/>
});


class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <div styleName="container">
          <Switch>
            <Route exact path="/" component={StaticPage}/>
            <Route path="/old/trade/:curr_pair" component={ExchangePageOld}/>
            <Route path="/trade/:curr_pair" component={ExchangePage}/>
            <Route path="/auth" component={AuthPage}/>
            <PrivateRoute path="/user" component={UserPage}/>
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default hot(module)(App);
