import React from 'react';
import { Link } from 'react-router-dom';
import { hot } from 'react-hot-loader';
import './styles.sss';

import Header from '@components/common/Header';

const StaticPage: React.SFC = () => <>
  <Header />
  <div styleName="static-page">
    <img styleName="logo" src={require('./images/logo.png')} alt=""/>
    <img styleName="name" src={require('./images/wf.png')} alt=""/>

    <p styleName="promo">Our world class trading platform <br/> is comming soon!</p>

    <div styleName="links">
      <Link styleName="link" to="/old/trade/BTCUSD">View old trading screen</Link>
      <Link styleName="link" to="/trade/BTCUSD">View new trading screen</Link>
    </div>
  </div>
</>;

export default hot(module)(StaticPage);
