import React, { PureComponent } from 'react';
import './styles.sss';

import TradingView from '@components/exchange/TradingView';
import OrderBook from '@components/exchange/OrderBook';
import History from '@components/exchange/History';
import OrderForm from '@components/exchange/OrderForm';
import OrderTable from '@components/exchange/OrderTable';


class LaptopScreen extends PureComponent {
  render() {
    return (
      <div styleName="wrap">
        <div styleName="container">
          <div styleName="chart">
            <TradingView />
          </div>
          <div styleName="order-book">
            <OrderBook />
          </div>
          <div styleName="trade-history">
            <History />
          </div>
          <div styleName="order-form">
            <OrderForm />
          </div>
        </div>

        <div styleName="tabs">
          <OrderTable />
        </div>
      </div>
    );
  }
}

export default LaptopScreen;
