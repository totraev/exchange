import React, { PureComponent } from 'react';
import './styles.sss';

import OrderBook from '@components/exchange/OrderBook';
import OrderForm from '@components/exchange/OrderForm';


class MobileLandscapeScreen extends PureComponent {
  render() {
    return (
      <div styleName="wrap">
        <div styleName="order-book">
          <OrderBook />
        </div>
        <div styleName="order-form">
          <OrderForm />
        </div>
      </div>
    );
  }
}

export default MobileLandscapeScreen;
