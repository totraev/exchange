import React, { PureComponent } from 'react';
import './styles.sss';

import OrderBook from '@components/exchange/OrderBook';
import OrderForm from '@components/exchange/OrderForm';
import OrderTable from '@components/exchange/OrderTable';
import TradingView from '@components/exchange/TradingView';

class TabletLandscapeScreen extends PureComponent {
  render() {
    return (
      <div styleName="wrap">
        <div styleName="container">
          <div styleName="trading">
            <div styleName="chart">
              <TradingView />
            </div>
            <div styleName="order-book">
              <OrderBook />
            </div>
          </div>

          <div styleName="tabs">
            <OrderTable />
          </div>
        </div>

        <div styleName="order-form">
          <OrderForm />
        </div>
      </div>
    );
  }
}

export default TabletLandscapeScreen;
