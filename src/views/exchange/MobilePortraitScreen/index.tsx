import React, { PureComponent } from 'react';
import './styles.sss';

import History from '@components/exchange/History';
import OrderBook from '@components/exchange/OrderBook';
import OrderForm from '@components/exchange/OrderForm';
import OrderTable from '@components/exchange/OrderTable';
import TradingView, { options as defaultOptions } from '@components/exchange/TradingView';

export interface State {
  activeSection: string;
}

class MobileScreen extends PureComponent<{}, State> {
  state: State = {
    activeSection: 'chart'
  };

  private isActive(name: string) {
    return name === this.state.activeSection
      ? 'section--active'
      : 'section';
  }

  private handleClick(name: string) {
    return () => this.setState({ activeSection: name });
  }

  render() {
    const { activeSection } = this.state;

    return <>
      <div styleName="mobile-portrait">
        <div styleName={this.isActive('chart')}>
          <TradingView options={{ ...defaultOptions, preset: 'mobile' }}/>
        </div>
        <div styleName={this.isActive('book')}>
          <OrderBook />
        </div>
        <div styleName={this.isActive('history')}>
          <History />
        </div>
        <div styleName={this.isActive('form')}>
          <OrderForm />
        </div>
        <div styleName={this.isActive('orders')}>
          <OrderTable />
        </div>
      </div>

      <div styleName="buttons">
        <button
          styleName={activeSection === 'chart' ? 'button--active' : 'button'}
          onClick={this.handleClick('chart')}
        >
          Chart
        </button>
        <button
          styleName={activeSection === 'book' ? 'button--active' : 'button'}
          onClick={this.handleClick('book')}
        >
          Book
        </button>
        <button
          styleName={activeSection === 'history' ? 'button--active' : 'button'}
          onClick={this.handleClick('history')}
        >
          History
        </button>
        <button
          styleName={activeSection === 'form' ? 'button--active' : 'button'}
          onClick={this.handleClick('form')}
        >
          Form
        </button>
        <button
          styleName={activeSection === 'orders' ? 'button--active' : 'button'}
          onClick={this.handleClick('orders')}
        >
          Oreders
        </button>
      </div>
    </>;
  }
}

export default MobileScreen;
