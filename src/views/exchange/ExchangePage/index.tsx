import React, { Component } from 'react';
import './styles.sss';

import MediaQuery from 'react-responsive';
import { hot } from 'react-hot-loader';

import MobilePortraitScreen from '@views/exchange/MobilePortraitScreen';
import MobileLandscapeScreen from '@views/exchange/MobileLandscapeScreen';
import TabletPortraitScreen from '@views/exchange/TabletPortraitScreen';
import TabletLandscapeScreen from '@views/exchange/TabletLandscapeScreen';
import LaptopScreen from '@views/exchange/LaptopScreen';

import Header from '@components/exchange/Header';
import Sidebar from '@components/exchange/Sidebar';


class ExchangePage extends Component {
  render() {
    return <div styleName="exchange-page">
      <Header/>
      <Sidebar/>

      <MediaQuery maxDeviceWidth={767} orientation="portrait">
        <MobilePortraitScreen />
      </MediaQuery>

      <MediaQuery minDeviceWidth={480} maxDeviceWidth={1023} orientation="landscape">
        <MobileLandscapeScreen />
      </MediaQuery>

      <MediaQuery minDeviceWidth={768} maxDeviceWidth={1024} orientation="portrait">
        <TabletPortraitScreen />
      </MediaQuery>

      <MediaQuery minDeviceWidth={1024} maxDeviceWidth={1223} orientation="landscape">
        <TabletLandscapeScreen />
      </MediaQuery>
      
      <MediaQuery minDeviceWidth={1224}>
        <LaptopScreen />
      </MediaQuery>
    </div>;
  }
}

export default hot(module)(ExchangePage);
