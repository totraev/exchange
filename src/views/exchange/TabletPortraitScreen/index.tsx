import React, { PureComponent } from 'react';
import './styles.sss';

import History from '@components/exchange/History';
import OrderBook from '@components/exchange/OrderBook';
import OrderForm from '@components/exchange/OrderForm';
import OrderTable from '@components/exchange/OrderTable';
import TradingView from '@components/exchange/TradingView';


class TabletPortraitScreen extends PureComponent {
  render() {
    return <>
      <div styleName="wrap">
        <div styleName="container">
          <div styleName="chart">
            <TradingView />
          </div>

          <div styleName="trading">
            <div styleName="order-book">
              <OrderBook />
            </div>
            <div styleName="trade-history">
              <History />
            </div>
          </div>
        </div>

        <div styleName="order-form">
          <OrderForm />
        </div>
      </div>

      <div styleName="tabs">
        <OrderTable />
      </div>
    </>;
  }
}

export default TabletPortraitScreen;
