import ApiCore from './ApiCore';

import {
  RegisterParams,
  LoginParams,
  LoginResponse
} from './types/authApi';

class AuthApi extends ApiCore {
  register(params: RegisterParams): Promise<void> {
    return this.publicApi
      .post('/users/register', params)
      .then(res => res.data);
  }

  login(params: LoginParams): Promise<LoginResponse> {
    return this.publicApi
      .post('/users/login', params)
      .then(res => res.data);
  }

  logout(): Promise<void> {
    return this.privateApi
      .post('/users/logout')
      .then(res => res.data);
  }
}

export default AuthApi;
