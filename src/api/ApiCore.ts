import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import RootStore from '../stores/RootStore';

import hmacSHA512 from 'crypto-js/hmac-sha512';
import Base64 from 'crypto-js/enc-base64';
import Utf8 from 'crypto-js/enc-utf8';
import Hex from 'crypto-js/enc-hex';


class ApiCore {
  publicApi: AxiosInstance = null;
  privateApi: AxiosInstance = null;

  constructor(private rootStore:  RootStore) {
    const { protocol, hostname, api_version } = this.rootStore.config;
    const baseURL = `${protocol}://${hostname}/${api_version}`;

    this.publicApi = axios.create({ baseURL });
    this.privateApi = axios.create({ baseURL });

    this.privateApi.interceptors.request.use(this.authInterceptor);
  }

  private authInterceptor = (cfg: AxiosRequestConfig): Partial<AxiosRequestConfig> => {
    const { authStore } = this.rootStore;
    const { api_version } = this.rootStore.config;

    if (!authStore.isAuth) {
      return cfg;
    }

    const data = {
      request: `/${api_version}${cfg.url}`,
      timestamp: Date.now() * 1000 * 1000,
      ...cfg.data
    };

    const payload = Base64.stringify(Utf8.parse(JSON.stringify(data)));

    return {
      ...cfg,
      data,
      headers: {
        APIKEY: authStore.apiKey,
        PAYLOAD: payload,
        SIGNATURE: Hex.stringify(hmacSHA512(payload, authStore.secret))
      }
    };
  }
}

export default ApiCore;

