export type Wallet = {
  curr: string
  address: string
};

export type Transaction = {
  trans_id: number,
  trans_type: 'withdrawal' | 'deposit',
  curr: string,
  amount: number,
  done: boolean,
  err_desc: string
};

export type Currency = {
  curr: string
};
