export type RegisterParams = {
  email: string
  password: string
  password_repeat: string
};

export type LoginParams = {
  email: string
  password: string
};

export type LoginResponse = {
  id: string
  secret: string
};
