export type SymbolRes = {
  curr_pair: string
};

export type SymbolDetail = {
  curr_pair: string
  price_step: string
  volume_step: string
  taker_fee: string
  maker_fee: string
};

export type Balance = {
  curr: string
  balance: string
  blocked: string
};

export type OrderPayload = {
  curr_pair: string
  volume: string
  price?: string
  side: 'sell' | 'buy'
  type: 'limit' | 'market'
};

export type OrderInfo = {
  order_id: number
  curr_pair: string
  side: 'sell' | 'buy'
  type: string
  price: string
  volume: string
  original_volume: string
  timestamp: number
  is_live: boolean
  is_canceled: boolean
};

export type OpenOrder = {
  order_id: number
  curr_pair: string
  side: 'sell' | 'buy'
  type: string
  price: string
  volume: string
  original_volume: string
  timestamp: number
  state: 'open' | 'canceled' | 'filled' | 'partial'
};

export type OrderRes = {
  order_id: string
};

export type CancelOrderPayload = {
  curr_pair: string
  order_id: string
  price: string
  side: 'sell' | 'buy'
};

export type OrderStatusPayload = { 
  curr_pair: string
  order_id: string
};
