export type KeyInfo = {
  id: string
  level: 1 | 2 | 3;
  secret: string
};
