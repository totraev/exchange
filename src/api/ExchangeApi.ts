import ApiCore from './ApiCore';

import {
  SymbolRes,
  SymbolDetail,
  Balance,
  OrderPayload,
  OrderRes,
  OrderInfo,
  CancelOrderPayload,
  OrderStatusPayload
} from './types/exchangeApi';


class ExchangeApi extends ApiCore {
  status(): Promise<void> {
    return this.publicApi
      .get<void>('/status')
      .then(res => res.data);
  }

  symbols(): Promise<SymbolRes[]> {
    return this.publicApi
      .get<SymbolRes[]>('/symbols')
      .then(res => res.data);
  }

  symbolDetails(): Promise<SymbolDetail[]> {
    return this.publicApi
      .get<SymbolDetail[]>('/symbol_details')
      .then(res => res.data);
  }

  balances(): Promise<Balance[]> {
    return this.privateApi
      .post<Balance[]>('/balances')
      .then(res => res.data);
  }

  createOrder(order: OrderPayload): Promise<OrderRes> {
    return this.privateApi
      .post<OrderRes>('/order/new', order)
      .then(res => res.data);
  }

  cancelOrder(order: CancelOrderPayload): Promise<void> {
    return this.privateApi
      .post<void>('/order/cancel', order)
      .then(res => res.data);
  }

  cancelAllOrders(currPair: string): Promise<void> {
    return this.privateApi
      .post<void>('/order/cancel/all', { curr_pair: currPair })
      .then(res => res.data);
  }

  orderStatus(order: OrderStatusPayload): Promise<OrderInfo> {
    return this.privateApi
      .post<OrderInfo>('/order/status', order)
      .then(res => res.data);
  }

  allOrders(): Promise<OrderInfo[]> {
    return this.privateApi
      .post<OrderInfo[]>('/orders')
      .then(res => res.data);
  }
}

export default ExchangeApi;
