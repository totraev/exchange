import ApiCore from './ApiCore';

import { Wallet, Transaction, Currency } from './types/walletsApi';


class WalletsApi extends ApiCore {
  create(curr: string): Promise<Wallet> {
    return this.privateApi
      .post('/wallets/generate', { curr })
      .then(res => res.data);
  }

  list(): Promise<Wallet[]> {
    return this.privateApi
      .post('/wallets/list')
      .then(res => res.data);
  }

  withdraw(curr: string, amount: number): Promise<void> {
    return this.privateApi
      .post('/wallets/withdraw', { curr, amount })
      .then(res => res.data);
  }

  transactions(): Promise<Transaction[]> {
    return this.privateApi
      .post('/wallets/transactions')
      .then(res => res.data);
  }

  currencies(): Promise<Currency[]> {
    return this.privateApi
      .post('/currencies')
      .then(res => res.data);
  }
}

export default WalletsApi;
