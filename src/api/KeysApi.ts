import ApiCore from './ApiCore';

import { KeyInfo } from './types/keysApi';

class KeysApi extends ApiCore {
  create(level: 1 | 2 | 3): Promise<KeyInfo> {
    return this.privateApi
      .post('/keys/new', { level })
      .then(res => res.data);
  }

  revoke(id: string): Promise<void> {
    return this.privateApi
      .post('/keys/revoke', { id })
      .then(res => res.data);
  }

  list(): Promise<KeyInfo[]> {
    return this.privateApi
      .post('/keys/list')
      .then(res => res.data);
  }
}

export default KeysApi;
