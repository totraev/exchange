import { observable, action } from 'mobx';
// import { RootStore } from './index';

class RestorePasswordStore {
  @observable step: 1 | 2 | 3 = 1;

  @action incrStep = () => {
    if (this.step < 3) {
      this.step += 1;
    }
  }
}

export default RestorePasswordStore;
