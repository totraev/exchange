import { observable, action, computed } from 'mobx';
import { asyncAction } from 'mobx-utils';
import BigNumber from 'bignumber.js';

import RootStore from './RootStore';
import { OpenOrder } from '../api/types/exchangeApi';
import {
  L3UpdateMessage,
  L3OrderExecutedPayload,
  L3OrderCanceledPayload,
  L3OrderCreatedPayload
} from '../ws/types/types';


class OpenOrdersStore {
  @observable orders = observable.map<string, OpenOrder>({});
  @observable loading: boolean = true;

  constructor(public rootStore: RootStore) {}

  @action startLoading() {
    this.loading = true;
  }

  @action stopLoading() {
    this.loading = false;
  }

  @action handleMessage = ({ type, payload }: L3UpdateMessage) => {
    if (type !== 'l3update') return;

    switch (payload.action) {
      case 1:
        return this.handleCreate(payload);
      case 2:
        return this.handleUpdate(payload);
      case 3:
        return this.handleClose(payload);
    }
  }

  @action private handleUpdate(order: L3OrderExecutedPayload) {
    const { order_id, curr_pair, volume, original_volume } = order;

    const orderInfo = this.orders.get(`${curr_pair}:${order_id}`);

    orderInfo.volume = volume;
    orderInfo.original_volume = original_volume;

    if (orderInfo.volume === '0') {
      orderInfo.state = 'filled';
    }
  }

  @action private handleClose(order: L3OrderCanceledPayload) {
    const { order_id, curr_pair, event_time } = order;

    const orderInfo = this.orders.get(`${curr_pair}:${order_id}`);

    if (orderInfo.volume !== orderInfo.original_volume) {
      orderInfo.state = 'partial';
    } else {
      orderInfo.state = 'canceled';
    }

    orderInfo.timestamp = event_time;
  }

  @action private handleCreate = (order: L3OrderCreatedPayload) => {
    const { order_id, curr_pair, side, type, price, volume, original_volume, event_time } = order;

    this.orders.set(`${curr_pair}:${order_id}`, {
      order_id,
      curr_pair,
      side,
      type,
      price,
      original_volume,
      volume,
      timestamp: event_time,
      state: 'open'
    });
  }

  @action handleSnapshot = (orders: L3OrderCreatedPayload[]) => {
    orders.forEach(this.handleCreate);
  }

  @asyncAction *cancelOrder(currPair: string, orderId: number) {
    const orderInfo = this.orders.get(`${currPair}:${orderId}`);

    try {
      if (orderInfo) {
        const { side, order_id, curr_pair, price } = orderInfo;

        yield this.rootStore.exchangeApi.cancelOrder({
          side,
          curr_pair,
          price,
          order_id: order_id.toString()
        });
      }
    } catch (e) {
      console.log(e);
    }
  }

  @asyncAction *cancelAllOrders() {
    const { currPair } = this.rootStore.appStore;

    try {
      yield this.rootStore.exchangeApi.cancelAllOrders(currPair);
    } catch (e) {
      console.log(e);
    }
  }

  @computed get hasOrders(): boolean {
    return this.currentOrders.length > 0;
  }

  @computed get currentOrders(): OpenOrder[] {
    const { currPair } = this.rootStore.appStore;
    const arr: OpenOrder[] = [];

    this.orders.forEach((order) => {
      if (order.curr_pair === currPair) {
        arr.push(order);
      }
    });

    return arr;
  }

  @computed get limitAskOrders(): Map<string, BigNumber> {
    const { currPair } = this.rootStore.appStore;
    const map = new Map<string, BigNumber>();

    this.orders.forEach((order) => {
      if (
        order.curr_pair === currPair &&
        order.type === 'limit' &&
        order.side === 'sell' &&
        order.state === 'open'
      ) {
        const price = new BigNumber(order.price);
        const size = new BigNumber(order.volume);
        const key = this.rootStore.orderBook.aggrPrice(price).toFixed(2);

        if (map.has(key)) {
          map.set(key, map.get(key).plus(size));
        } else {
          map.set(key, size);
        }
      }
    });

    return map;
  }

  @computed get limitBidOrders(): Map<string, BigNumber> {
    const { currPair } = this.rootStore.appStore;
    const map = new Map<string, BigNumber>();

    this.orders.forEach((order) => {
      if (
        order.curr_pair === currPair &&
        order.type === 'limit' &&
        order.side === 'buy' &&
        order.state === 'open'
      ) {
        const price = new BigNumber(order.price);
        const size = new BigNumber(order.volume);
        const key = this.rootStore.orderBook.aggrPrice(price).toFixed(2);

        if (map.has(key)) {
          map.set(key, map.get(key).plus(size));
        } else {
          map.set(key, size);
        }
      }
    });

    return map;
  }
}

export default OpenOrdersStore;
