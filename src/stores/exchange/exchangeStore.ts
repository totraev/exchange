import { observable, action } from 'mobx';

import chartStore from './chartStore';

export class ExchangeStore {
  chartStore = chartStore;

  @observable sidebar: boolean = false;

  @action toggleSidebar = () => {
    this.sidebar = !this.sidebar;
  }
}

export type InjectedExchangeStore = {
  exchangeStore?: ExchangeStore
};

export default new ExchangeStore();
