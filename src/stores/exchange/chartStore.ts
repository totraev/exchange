import { observable, action, computed } from 'mobx';

export interface CurrPair {
  name: string;
  favorite: boolean;
  price: number;
  change: string;
}

export class ChartStore {
  @observable search = {
    active: false,
    value: '',
    hovered: -1
  };
  @observable currPairs: CurrPair[] = [
    { name: 'BTCETH', favorite: true, price: 0.123567, change: '+1.30' },
    { name: 'BTCLTC', favorite: true, price: 0.123567, change: '+1.30' },
    { name: 'BTCETC', favorite: true, price: 0.123567, change: '+1.30' },
    { name: 'BTCEOS', favorite: true, price: 0.123567, change: '+1.30' },
    { name: 'BTCOMG', favorite: false, price: 0.123567, change: '+1.30' },
    { name: 'BTCXRP', favorite: false, price: 0.123567, change: '+1.30' },
    { name: 'BTCDASH', favorite: false, price: 0.123567, change: '+1.30' },
    { name: 'BTCZEC', favorite: false, price: 0.123567, change: '+1.30' },
    { name: 'BTCIOTA', favorite: false, price: 0.123567, change: '+1.30' },
    { name: 'BTCADA', favorite: false, price: 0.123567, change: '+1.30' },
    { name: 'BTCSTEEM', favorite: false, price: 0.123567, change: '+1.30' },
    { name: 'BTCBCH', favorite: false, price: 0.123567, change: '+1.30' }
  ];
  @observable selectedCurr: CurrPair = this.currPairs[0];

  @action activateSearch = () => {
    this.search.active = true;
  }

  @action deactivateSearch = () => {
    this.search.active = false;
  }

  @action handleChange = (value: string) => {
    this.search.value = value;
    this.search.hovered = -1;
  }

  @action handleNextHovered = () => {
    this.search.hovered < this.filteredCurrPairs.length - 1
      ? this.search.hovered += 1
      : this.search.hovered = 0;
  }

  @action handlePrevHovered = () => {
    this.search.hovered > 0
      ? this.search.hovered -= 1
      : this.search.hovered = this.filteredCurrPairs.length - 1;
  }

  @action selectPair = (index: number) => {
    if (index >= 0 && index < this.filteredCurrPairs.length) {
      this.selectedCurr = this.filteredCurrPairs[index];
      this.search = {
        active: false,
        value: '',
        hovered: -1
      };
    }
  }

  @computed get filteredCurrPairs(): CurrPair[] {
    const regExp = RegExp(`^${this.search.value}`, 'i');

    return this.currPairs.filter(currPair => 
      regExp.test(currPair.name) && currPair !== this.selectedCurr);
  }
}

export default new ChartStore();
