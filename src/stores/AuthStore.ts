import { observable, computed } from 'mobx';
import { asyncAction } from 'mobx-utils';
import RootStore from './RootStore';

export type RegisterParams = {
  email: string
  password: string
  repeatPassword: string
};


export class AuthStore {
  @observable apiKey: string = localStorage.getItem('apiKey');
  @observable secret: string = localStorage.getItem('secret');

  constructor(public rootStore: RootStore) {}

  @asyncAction *register(params: RegisterParams) {
    const { email, password, repeatPassword } = params;

    yield this.rootStore.authApi.register({
      email,
      password,
      password_repeat: repeatPassword
    });
  }

  @asyncAction *login(email: string, password: string) {
    const { id: apiKey, secret } = yield this.rootStore.authApi.login({ email, password });

    this.apiKey = apiKey;
    this.secret = secret;

    localStorage.setItem('apiKey', apiKey);
    localStorage.setItem('secret', secret);
  }

  @asyncAction *logout() {
    try {
      yield this.rootStore.authApi.logout();

      this.apiKey = null;
      this.secret = null;

      localStorage.removeItem('apiKey');
      localStorage.removeItem('secret');
    } catch (e) {
      console.log(e);
    }
  }

  @computed get isAuth(): boolean {
    return Boolean(this.apiKey) && Boolean(this.secret);
  }

  @computed get authData() {
    return this.isAuth
    ? {
      apikey: this.apiKey,
      secret: this.secret
    }
    : null;
  }
}

export default AuthStore;
