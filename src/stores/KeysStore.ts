import { observable, action } from 'mobx';
import { asyncAction } from 'mobx-utils';

import RootStore from './RootStore';
import { KeyInfo } from '../api/types/keysApi';

export type Key = KeyInfo & {
  showSecret: boolean;
};

class KeysStore {
  @observable list: Key[] = [];
  @observable keyLevel: 1 | 2 | 3 = 1;
  @observable btnDisabled: boolean = false;
  @observable loading: boolean = false;

  constructor(public rootStore: RootStore) {}

  @action toggleKeyLevel(value: 1 | 2 | 3) {
    this.keyLevel = value;
  }

  @action toggleSecret(id: string) {
    const key = this.list.find(key => key.id === id);
    key.showSecret = !key.showSecret;
  }

  @asyncAction *getKeys() {
    this.loading = true;

    try {
      const keys = yield this.rootStore.keysApi.list();
      this.list = keys.map(key => ({ ...key, showSecret: false }));
    } catch (e) {
      console.log(e);
    } finally {
      this.loading = false;
    }
  }

  @asyncAction *createKey() {
    this.btnDisabled = true;

    try {
      const key: KeyInfo = yield this.rootStore.keysApi.create(this.keyLevel);
      this.list.push({ ...key, showSecret: false });
    } catch (e) {
      console.log(e);
    } finally {
      this.btnDisabled = false;
    }
  }

  @asyncAction *revokeKey(id: string) {
    try {
      const remove = confirm('Are you sure?');

      if (remove) {
        this.rootStore.keysApi.revoke(id);
        const index = this.list.findIndex(key => key.id === id);

        if (index >= 0) {
          this.list.splice(index, 1);
        }
      }
    } catch (e) {
      console.log(e);
    }
  }
}

export default KeysStore;
