import { observable, action, computed } from 'mobx';
import { asyncAction } from 'mobx-utils';
import RootStore from './RootStore';

import { Wallet } from '../api/types/walletsApi';

export type WalletFields = {
  curr: string
};

class WalletsStore {
  @observable wallets: Wallet[] = [];
  @observable loading: boolean = false;
  @observable fields: WalletFields = {
    curr: ''
  };

  constructor(private rootStore: RootStore) {}

  @action loadingStart() {
    this.loading = true;
  }

  @action loadingEnd() {
    this.loading = false;
  }

  @action updateField(name: keyof WalletFields, value: string) {
    this.fields[name] = value;
  }

  @asyncAction *loadWallets() {
    this.loadingStart();

    try {
      this.wallets = yield this.rootStore.walletsApi.list();
    } catch (e) {
      console.log(e);
    } finally {
      this.loadingEnd();
    }
  }

  @asyncAction *createWallet() {
    const { curr } = this.fields;

    try {
      const wallet: Wallet = yield this.rootStore.walletsApi.create(curr);

      const index = this.wallets.findIndex(({ curr }) => wallet.curr === curr);
      this.wallets[index].address = wallet.address;
    } catch (e) {
      console.log(e);
    }
  }

  @computed get isValid(): boolean {
    return this.fields.curr !== '';
  }
}

export default WalletsStore;
