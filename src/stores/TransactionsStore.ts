import { observable, action, computed } from 'mobx';
import { asyncAction } from 'mobx-utils';
import RootStore from './RootStore';

import { Transaction, Wallet } from '../api/types/walletsApi';

export type WithdrawalFields = {
  curr: string
  amount: string
};


class TransactionsStore {
  @observable wallets: Wallet[] = [];
  @observable transactions: Transaction[] = [];
  @observable loading: boolean = false;
  @observable fields: WithdrawalFields = {
    curr: '',
    amount: ''
  };

  constructor(private rootStore: RootStore) {}

  @action loadingStart() {
    this.loading = true;
  }

  @action loadingEnd() {
    this.loading = false;
  }

  @action updateField(name: keyof WithdrawalFields, value: string) {
    if (name === 'curr') {
      this.fields[name] = value;
    } else {
      this.fields[name] = /^\d*\.?\d*$/.test(value)
        ? value
        : this.fields[name];
    }
  }

  @asyncAction *loadWallets() {
    try {
      this.wallets = yield this.rootStore.walletsApi.list();
    } catch (e) {
      console.log(e);
    }
  }

  @asyncAction *loadTransactions() {
    this.loadingStart();

    try {
      this.transactions = yield this.rootStore.walletsApi.transactions();
    } catch (e) {
      console.log(e);
    } finally {
      this.loadingEnd();
    }
  }

  @asyncAction *withdraw() {
    const { curr, amount } = this.fields;

    try {
      yield this.rootStore.walletsApi.withdraw(curr, parseFloat(amount));
    } catch (e) {
      console.log(e);
    }
  }

  @computed get isValid(): boolean {
    return this.fields.curr !== '' && this.fields.amount !== '';
  }
}

export default TransactionsStore;
