import { observable, action, computed } from 'mobx';
import { asyncAction } from 'mobx-utils';
import BigNumber from 'bignumber.js';

import RootStore from './RootStore';


class OrderFormStore {
  @observable type: 'market' | 'limit' = 'market';
  @observable side: 'buy' | 'sell' = 'buy';
  @observable amount: string = '';
  @observable price: string = '';
  @observable disabled: boolean = false;

  constructor(public rootStore: RootStore) {}

  @action setType(type: 'market' | 'limit') {
    this.type = type;
    this.amount = '';
    this.price = '';
  }

  @action setSide(side: 'buy' | 'sell') {
    this.side = side;
  }

  @action updateAmount(value: string) {
    this.amount = this.isValidNumber(value) ? value : this.amount;
  }

  @action updatePrice(value: string) {
    this.price = this.isValidNumber(value) ? value : this.price;
  }

  @action resetForm() {
    this.price = '';
    this.amount = '';
  }

  @asyncAction *createOrder () {
    const { exchangeApi } = this.rootStore;
    this.disabled = true;

    try {
      yield exchangeApi.createOrder(this.orderFormData);
      this.resetForm();
    } catch (e) {
      console.log(e);
    } finally {
      this.disabled = false;
    }
  }

  @computed get totalMarket(): BigNumber {
    const { orderBook } = this.rootStore;

    if (orderBook === null) {
      return new BigNumber(0);
    }

    const value = Boolean(this.amount)
      ? new BigNumber(this.amount)
      : new BigNumber(0);

    return this.side === 'buy'
      ? orderBook.marketBuyPrice(value)
      : orderBook.marketSellPrice(value);
  }

  @computed get totalLimit(): BigNumber {
    return Boolean(this.price) && Boolean(this.amount)
      ? (new BigNumber(this.price)).multipliedBy(new BigNumber(this.amount))
      : new BigNumber(0);
  }

  @computed get total(): string {
    return this.amount === '' ? '0.00' : this.type === 'market'
      ? this.totalMarket.toFixed(2)
      : this.totalLimit.toFixed(2);
  }

  @computed get isLimitFormValid(): boolean {
    return Boolean(this.price) && Boolean(this.amount);
  }

  @computed get isMarketFormValid(): boolean {
    return Boolean(this.amount);
  }

  @computed get isDisabled(): boolean {
    return this.type === 'market'
      ? this.disabled || !this.isMarketFormValid
      : this.disabled || !this.isLimitFormValid;
  }

  get orderFormData() {
    const { appStore } =  this.rootStore;

    return this.type === 'limit'
      ? {
        curr_pair: appStore.currPair,
        volume: this.amount,
        price: this.price,
        side: this.side,
        type: this.type
      }
      : {
        curr_pair: appStore.currPair,
        volume: this.amount,
        side: this.side,
        type: this.type
      };
  }

  private isValidNumber(value: string): boolean {
    return /^\d*\.?\d*$/.test(value);
  }
}

export default OrderFormStore;
