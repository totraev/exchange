import { observable, action, computed, createAtom } from 'mobx';
import BigNumber from 'bignumber.js';
import { RBTree } from 'bintrees';

import RootStore from './RootStore';
import { Order, OrderInfo } from '../ws/types/types';


class OrderBook {
  private bidsTree = new RBTree<Order>((a, b) => a.price.comparedTo(b.price));
  private asksTree = new RBTree<Order>((a, b) => a.price.comparedTo(b.price));
  private atom = createAtom(
    'OrderBook',
    () => { console.log('start'); },
    () => { console.log('end'); }
  );

  @observable currPair: string = '';
  @observable ordersLimit: number = 50;
  @observable aggregation = {
    values: [0.01, 0.05, 0.1, 0.25, 0.5, 1, 2.5, 5],
    step: 0
  };

  constructor(public rootStore: RootStore) {}

  @action setCurrPair(currPair: string) {
    this.currPair = currPair;
    this.clear();
  }

  @action updateOrders(orders: OrderInfo[]) {
    orders.forEach((orderInfo) => {
      const { side, price, volume, timestamp } = orderInfo;

      const order = {
        side,
        timestamp,
        price: new BigNumber(price),
        size: new BigNumber(volume),
        id: price
      };

      this.update(order);
    });
  }

  @action incAggr = () => {
    const { step, values } = this.aggregation;
    this.aggregation.step = step < values.length - 1 ? step + 1 : step;
  }

  @action decAggr = () => {
    const { step } = this.aggregation;
    this.aggregation.step = step > 0 ? step - 1 : step;
  }

  @action setOrdersLimit = (value: number) => {
    this.ordersLimit = value;
  }

  @computed get aggrValue() {
    const { values, step } = this.aggregation;
    return values[step];
  }

  @computed get isIncBtnDisabled() {
    const { values, step } = this.aggregation;
    return step === values.length - 1;
  }

  @computed get isDecBtnDisabled() {
    return this.aggregation.step === 0;
  }

  get spread(): string {
    this.atom.reportObserved();

    if (this.asksTree.min() === null || this.bidsTree.max() === null) {
      return 'N/A';
    }

    return this.asksTree.min().price.minus(this.bidsTree.max().price).toFixed(2);
  }

  aggrPrice(price: BigNumber): BigNumber {
    this.atom.reportObserved();

    return price
      .dividedBy(this.aggrValue)
      .integerValue()
      .multipliedBy(this.aggrValue);
  }

  get asks() {
    this.atom.reportObserved();

    const arr: Order[] = [];
    let prevOrder: Order = null;

    this.asksTree.each((order) => {
      if (prevOrder === null || prevOrder.price.minus(order.price).abs().gte(this.aggrValue)) {
        const newPrice = this.aggrPrice(order.price);

        const newOrder = {
          id: newPrice.toFixed(2),
          side: order.side,
          price: newPrice,
          size: order.size,
          timestamp: order.timestamp
        };

        arr.push(newOrder);
        prevOrder = newOrder;
      } else {
        prevOrder.size = prevOrder.size.plus(order.size);
        prevOrder.timestamp = order.timestamp;
      }

      return this.ordersLimit > arr.length;
    });

    return arr.reverse();
  }

  get bids() {
    this.atom.reportObserved();

    const arr: Order[] = [];
    let prevOrder: Order = null;

    this.bidsTree.reach((order) => {
      if (prevOrder === null || prevOrder.price.minus(order.price).abs().gte(this.aggrValue)) {
        const newPrice = this.aggrPrice(order.price);

        const newOrder = {
          id: newPrice.toFixed(2),
          side: order.side,
          price: newPrice,
          size: order.size,
          timestamp: order.timestamp
        };

        arr.push(newOrder);
        prevOrder = newOrder;
      } else {
        prevOrder.size = prevOrder.size.plus(order.size);
        prevOrder.timestamp = order.timestamp;
      }

      return this.ordersLimit > arr.length;
    });

    return arr;
  }

  private getTree(side: 'buy' | 'sell'): RBTree<Order> {
    return side === 'buy' ? this.bidsTree : this.asksTree;
  }

  private update(order: Order): void {
    if (order.size.isZero()) {
      this.remove(order);
    } else {
      this.add(order);
    }
  }

  private add(order: Order): void {
    const tree = this.getTree(order.side);
    const node = tree.find(order);

    if (node) {
      node.size = order.size;
    } else {
      tree.insert(order);
    }

    this.atom.reportChanged();
  }

  private remove(order: Order): void {
    this.getTree(order.side).remove(order);
    this.atom.reportChanged();
  }

  private clear() {
    this.asksTree.clear();
    this.bidsTree.clear();

    this.atom.reportChanged();
  }
  
  marketBuyPrice(amount: BigNumber): BigNumber {
    this.atom.reportObserved();
  
    let amountSize = amount;
    let price = new BigNumber(0);

    this.asksTree.each((item) => {
      const size = item.size.gte(amountSize) ? amountSize : item.size;
      const total = (new BigNumber(size)).multipliedBy(item.price);

      price = price.plus(total);
      amountSize = amountSize.minus(size);

      return amountSize.gt(0);
    });

    return price;
  }

  marketSellPrice(amount: BigNumber): BigNumber {
    this.atom.reportObserved();

    let amountSize = amount;
    let price = new BigNumber(0);

    this.bidsTree.reach((item) => {
      const size = item.size.gte(amountSize) ? amountSize : item.size;
      const total = (new BigNumber(size)).multipliedBy(item.price);

      price = price.plus(total);
      amountSize = amountSize.minus(size);

      return amountSize.gt(0);
    });

    return price;
  }
}

export default OrderBook;
