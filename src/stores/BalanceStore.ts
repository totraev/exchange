import { observable, computed } from 'mobx';
import { asyncAction } from 'mobx-utils';
import RootStore from './RootStore';

import { Balance } from '../api/types/exchangeApi';


export class BalanceStore {
  @observable userBalanceMap = observable.map<string, Balance>({});
  @observable loading: boolean = false;

  constructor(public rootStore: RootStore) {}

  @asyncAction *fetchUserBalances() {
    this.loading = true;

    try {
      const userBalances: Balance[] = yield this.rootStore.exchangeApi.balances();
  
      userBalances.forEach(balance =>
          this.userBalanceMap.set(balance.curr, balance));
    } catch (e) {
      console.log(e);
    } finally {
      this.loading = false;
    }
  }

  @computed get currentCurrBalances(): Balance[] {
    return this.rootStore.appStore.currentCurrencies
      .filter(curr => this.userBalanceMap.get(curr))
      .map(curr => this.userBalanceMap.get(curr));
  }
}

export default BalanceStore;
