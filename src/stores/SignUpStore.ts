import { observable, action } from 'mobx';
import { asyncAction } from 'mobx-utils';
import RootStore from './RootStore';

import Validator from 'validatorjs';


export type SignUpFields = {
  email: string
  password: string
  repeatPassword: string
};


class SignUpStore {
  @observable step: 1 | 2 | 3 = 1;
  @observable fields: SignUpFields = {
    email: '',
    password: '',
    repeatPassword: ''
  };
  @observable loading: boolean = false;
  @observable.ref errors: string[] = [];

  constructor(private rootStore: RootStore) {}

  @action incrStep = () => {
    if (this.step < 3) {
      this.step += 1;
    }
  }

  @action updateField(name: keyof SignUpFields, value: string) {
    this.fields[name] = value;
  }

  @action loadingStart() {
    this.loading = true;
  }

  @action loadingEnd() {
    this.loading = false;
  }

  @action resetState() {
    this.step = 1;
    this.loading = false;
    this.fields = {
      email: '',
      password: '',
      repeatPassword: ''
    };
    this.errors = [];
  }

  @asyncAction *handleSubmit() {
    this.loadingStart();

    try {
      const validation = new Validator(this.fields, {
        email: 'required|email',
        password: 'required|min:6',
        repeatPassword: 'required|same:password'
      });
  
      if (validation.passes() as boolean) {
        yield this.rootStore.authStore.register(this.fields);
  
        this.incrStep();
        this.incrStep();
      } else {
        this.errors = [
          ...validation.errors.get('email'),
          ...validation.errors.get('password'),
          ...validation.errors.get('repeatPassword')
        ];
      }  
    } catch (e) {
      this.errors = [e.message];
    }

    this.loadingEnd();
  }
}

export default SignUpStore;
