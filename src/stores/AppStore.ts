import { observable, action, computed } from 'mobx';
import { asyncAction } from 'mobx-utils';

import RootStore from './RootStore';
import { SymbolRes as Symbol } from '../api/types/exchangeApi';

export class AppStore {  
  @observable loading: boolean = true;
  @observable currPair: string = '';
  @observable symbols: Symbol[] = [];

  constructor(public rootStore: RootStore) {}

  @asyncAction *fetchCurrPairs() {
    try {
      this.symbols = yield this.rootStore.exchangeApi.symbols();
    } catch (e) {
      console.log(e);
    }
  }

  @action loadEnd() {
    this.loading = false;
  }

  @action loadStart() {
    this.loading = true;
  }

  @action.bound setCurrPair(currPair: string) {
    this.currPair = currPair;
  }

  @computed get currentCurrencies(): [string, string] {
    return this.currPair !== ''
      ? [this.currPair.substr(0, 3), this.currPair.substr(3)]
      : ['', ''];
  }

  @computed get currPairs(): string[] {
    return this.symbols.map(({ curr_pair }) => curr_pair); 
  }
}

export default AppStore;
