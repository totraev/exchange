import { observable, action } from 'mobx';
import { asyncAction } from 'mobx-utils';
import RootStore from './RootStore';

import Validator from 'validatorjs';

export type SignInFields = {
  email: string
  password: string
};


class SignInStore {
  @observable step: 1 | 2 = 1;
  @observable loading: boolean = false;

  @observable fields: SignInFields = {
    email: '',
    password: ''
  };
  @observable.ref errors: string[] = [];

  constructor(private rootStore: RootStore) {}

  @action incrStep = () => {
    if (this.step < 2) {
      this.step += 1;
    }
  }

  @action updateField(name: keyof SignInFields, value: string) {
    this.fields[name] = value;
  }

  @action loadingStart() {
    this.loading = true;
  }

  @action loadingEnd() {
    this.loading = false;
  }

  @action resetState() {
    this.step = 1;
    this.loading = false;
    this.fields = {
      email: '',
      password: ''
    };
    this.errors = [];
  }

  @asyncAction *handleSubmit() {
    this.loadingStart();
    const { email, password } = this.fields;

    try {
      const validation = new Validator({ email, password }, {
        email: 'required|email',
        password: 'required|min:6'
      });

      if (validation.passes() as boolean) {
        yield this.rootStore.authStore.login(email, password);
        this.incrStep();
      } else {
        this.errors = [
          ...validation.errors.get('email'),
          ...validation.errors.get('password')
        ];
      }
    } catch (e) {
      this.errors = [e.message];
    } finally {
      this.loadingEnd();
    }
  }
}

export default SignInStore;
