import AppStore from './AppStore';
import OrderBook from './OrderBook';
import OrderFormStore from './OrderFormStore';
import AuthStore from './AuthStore';
import OpenOrdersStore from './OpenOrdersStore';
import BalanceStore from './BalanceStore';
import SignUpStore from './SignUpStore';
import SignInStore from './SignInStore';
import RestorePasswordStore from './RestorePasswordStore';
import KeysStore from './KeysStore';
import WalletsStore from './WalletsStore';
import TransactionsStore from './TransactionsStore';

import ExchangeApi from '../api/ExchangeApi';
import KeysApi from '../api/KeysApi';
import AuthApi from '../api/AuthApi';
import walletsApi from '../api/WalletsApi';
import L2UpdatesClient from '../ws/L2UpdatesClient';
import L3UserUpdatesClient from '../ws/L3UserUpdatesClient';

import config from '../config';

export type InjectStoreProps = {
  rootStore?: RootStore
};

class RootStore {
  config = config;

  appStore = new AppStore(this);
  orderFormStore = new OrderFormStore(this);
  authStore = new AuthStore(this);
  openOrdersStore = new OpenOrdersStore(this);
  balanceStore = new BalanceStore(this);
  orderBook = new OrderBook(this);

  keysStore = new KeysStore(this);
  walletsStore = new WalletsStore(this);
  transactionsStore = new TransactionsStore(this);

  signUpStore = new SignUpStore(this);
  signInStore = new SignInStore(this);
  restorePasswordStore = new RestorePasswordStore();

  exchangeApi = new ExchangeApi(this);
  keysApi = new KeysApi(this);
  authApi = new AuthApi(this);
  walletsApi = new walletsApi(this);
  l2UpdatesClient = new L2UpdatesClient(this);
  l3UserUpdatesClient = new L3UserUpdatesClient(this);
}

export default RootStore;

