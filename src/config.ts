const hostname = window.location.port !== undefined
  ? `${window.location.hostname}:${window.location.port}`
  : window.location.hostname;

const {
  PROTOCOL = 'http',
  HOSTNAME = hostname,
  API_VERSION = 'v1'
} = process.env;

export default {
  protocol: PROTOCOL,
  hostname: HOSTNAME,
  api_version: API_VERSION
};
