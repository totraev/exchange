import { Order } from '../../ws/types/types';
import { PriceLevel } from './OrderRaw';
import BigNumber from 'bignumber.js';


export default class OrderTable {
  private ctx: CanvasRenderingContext2D = null;
  private priceLevelMap = new Map<string, PriceLevel>();
  private priceLevels: PriceLevel[] = [];
  private userOrdersMap = new Map<string, BigNumber>();

  constructor(
    private canvas: HTMLCanvasElement = null,
    private initRenderHandler = () => {},
    width: number = 0,
    height: number = 0
  ) {
    this.width = width;
    this.canvas.height = height;
    this.ctx = canvas.getContext('2d');
  }

  getPriceLevels(): PriceLevel[] {
    return this.priceLevels;
  }

  setUserOrdersMap(map: Map<string, BigNumber>) {
    this.userOrdersMap = map;

    this.priceLevels.forEach((priceLevel, i) => {
      const key = priceLevel.price.toFixed(2);

      if (map.has(key)) {
        priceLevel.userOrderAmount = map.get(key);
        priceLevel.drawRaw(i, true);
      }
    });
  }

  set width(val: number) {
    this.canvas.width = val;
    PriceLevel.options.width = val;
  }

  draw(orders: Order[]) {
    this.canvas.height = orders.length * PriceLevel.options.height;

    if (orders.length > 0 && this.priceLevels.length === 0) {
      this.initRenderHandler();
    }

    if (orders.length === 0) {
      this.priceLevels = [];
    }

    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.mergeOrders(orders);
    this.priceLevels.forEach((priceLevel, i) => priceLevel.drawRaw(i, false));
  }

  mergeOrders(next: Order[]): void {
    let i = 0;
    let j = 0;
    const arr: PriceLevel[] = [];

    this.priceLevelMap.clear();
   
    if (next.length === 0) {
      return;
    }
    
    next.forEach((order) => {
      const key = order.price.toFixed(2);
      const priceLevel = new PriceLevel(this.ctx, order);

      if (this.userOrdersMap.has(key)) {
        priceLevel.userOrderAmount = this.userOrdersMap.get(key);
      }

      this.priceLevelMap.set(key, priceLevel);
    });

    if (this.priceLevels.length === 0) {
      this.priceLevels = next.map(order => this.priceLevelMap.get(order.id));
      return;
    }
    
    while (i < next.length || j < this.priceLevels.length) {
      let priceLevel = null;
      const newPriceLevel = i < next.length
        ? this.priceLevelMap.get(next[i].price.toFixed(2))
        : null;
      const prevPriceLevel = j < this.priceLevels.length ? this.priceLevels[j] : null;
      
      if (newPriceLevel !== null && prevPriceLevel !== null) {
        if (newPriceLevel.price.gt(prevPriceLevel.price)) {
          priceLevel = newPriceLevel;
          i += 1;
        } else if (prevPriceLevel.price.gt(newPriceLevel.price)) {
          priceLevel = prevPriceLevel;

          if (!this.priceLevelMap.has(prevPriceLevel.price.toFixed(2))) {
            priceLevel.state = 'remove';
          }

          j += 1;
        } else {
          newPriceLevel.state = prevPriceLevel.state;
          priceLevel = newPriceLevel;
          i += 1;
          j += 1;
        }
      } else if (newPriceLevel !== null) {
        priceLevel = newPriceLevel;
    
        i += 1;
      } else if (prevPriceLevel !== null) {
        priceLevel = prevPriceLevel;
  
        if (!this.priceLevelMap.has(prevPriceLevel.price.toFixed(2))) {
          priceLevel.state = 'remove';
        }
  
        j += 1;
      }
      
      if (priceLevel.state !== 'remove') {
        arr.push(priceLevel);
      }
    }
    
    this.priceLevels = arr;
  }
}
