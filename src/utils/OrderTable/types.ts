export type PriceLevelOptions = {
  height: number
  width: number
  font: {
    size: number
    family: string
  }
  padding: {
    leftRight: number
    topBottom: number
  }
  size: {
    color: string
    zeroesColor: string
    textAlign: 'left' | 'center' | 'right'
  }
  price: {
    buy: {
      color: string
      decimalColor: string
      textAlign: 'left' | 'center' | 'right'
    }
    sell: {
      color: string
      decimalColor: string
      textAlign: 'left' | 'center' | 'right'
    }
  }
  total: {
    color: string
    textAlign: 'left' | 'center' | 'right'
  }
};
