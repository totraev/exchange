import BigNumber from 'bignumber.js';
import { PriceLevelOptions } from './types';
import { Order } from'../../ws/types/types';


export class PriceLevel {
  static options: PriceLevelOptions;

  price = new BigNumber(0);
  size = new BigNumber(0);
  side: 'buy' | 'sell' = 'buy';
  userOrderAmount = new BigNumber(0);
  state: 'new' | 'old' | 'removed' | 'deleted' = 'new';
  position: number = 0;

  constructor(public ctx: CanvasRenderingContext2D, order: Order = null) {
    if (order !== null) {
      this.price = order.price;
      this.size = order.size;
      this.side = order.side;
    }
  }

  drawRaw(position: number, clearRow: boolean = true) {
    this.position = position;

    if (clearRow) {
      this.ctx.clearRect(
        0,
        position * PriceLevel.options.height,
        PriceLevel.options.width,
        PriceLevel.options.height
      );
    }

    this.drawSize();
    this.drawPrice();
    this.drawTotal();
  }

  private drawSize() {
    const { font, padding, size: sizeOpt, height } = PriceLevel.options;
    const [size, sizeValue] = this.size
      .toFixed(8)
      .match(/^(\d+\.(?:\d*[1-9])?)0*$/);
    const sizeX = padding.leftRight;
    const sizeY = height / 2;
    const topOffset = height * this.position;

    const options = {
      font: `${font.size}px "${font.family}"`,
      color: sizeOpt.zeroesColor,
      align: sizeOpt.textAlign,
      baseline: 'middle'
    };

    this.drawText(size, sizeX, sizeY + topOffset, options);
    options.color = sizeOpt.color;
    this.drawText(sizeValue, sizeX, sizeY + topOffset, options);
  }

  private drawPrice() {
    const { font, price: priceOpt, height, width } = PriceLevel.options;
    const price = this.price.toFixed(2);
    const [, dec] = price.split('.');

    const priceW = this.ctx.measureText(price).width / 2;
    const sizeX = width / 2 + priceW;
    const sizeY = height / 2;
    const topOffset = height * this.position;
    
    const options = {
      font: `${font.size}px "${font.family}"`,
      color: priceOpt[this.side].color,
      align: priceOpt[this.side].textAlign,
      baseline: 'middle'
    };

    this.drawText(price, sizeX, sizeY + topOffset, options);
    options.color = priceOpt[this.side].decimalColor;
    this.drawText(dec, sizeX, sizeY + topOffset, options);
  }

  private drawTotal() {
    const { font, padding, total: totalOpt, width, height } = PriceLevel.options;
    const total = this.userOrderAmount.gt(0)
      ? this.userOrderAmount.toString()
      : '-';

    const sizeX = width - padding.leftRight;
    const sizeY = height / 2;
    const topOffset = height * this.position;

    const options = {
      font: `${font.size}px "${font.family}"`,
      color: totalOpt.color,
      align: totalOpt.textAlign,
      baseline: 'middle'
    };

    this.drawText(total, sizeX, sizeY + topOffset, options);
  }

  private drawText(text: string, x: number, y: number, options: any) {
    const { font, color, align, baseline } = options;

    this.ctx.font = font;
    this.ctx.fillStyle = color;
    this.ctx.textAlign = align;
    this.ctx.textBaseline = baseline;
    this.ctx.fillText(text, x, y);
  }
}

PriceLevel.options = {
  height: 18,
  width: 0,
  font: {
    size: 12,
    family: 'Open Sans'
  },
  padding: {
    leftRight: 20,
    topBottom: 3,
  },
  size: {
    color: '#ffffff',
    zeroesColor: '#5a6469',
    textAlign: 'left'
  },
  price: {
    buy: {
      color: '#618b4c',
      decimalColor: '#a2e667',
      textAlign: 'right'
    },
    sell: {
      color: '#93503a',
      decimalColor: '#ff7b54',
      textAlign: 'right'
    }
  },
  total: {
    color: '#ffffff',
    textAlign: 'right'
  }
};
