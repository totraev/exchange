require('dotenv').config();

const bs = require('browser-sync').create();
const historyApiFallback = require('connect-history-api-fallback');
const proxy = require('http-proxy-middleware');

bs.init({
  port: 4000,
  ui: {
    port: 4001
  },
  server: {
    baseDir: 'dist',

    middleware: [
      proxy('/v1', { 
        target: process.env.STAGE_URL || 'http://159.65.66.180',
        changeOrigin: true,
        ws: true,
        auth: process.env.BASIC_AUTH,
        logLevel: 'debug'
      }),
      historyApiFallback(),
    ]
  },
  files: [
    'dist/*.html'
  ]
});
