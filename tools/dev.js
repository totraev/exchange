process.env.NODE_ENV = 'development';
require('dotenv').config();

const webpack = require('webpack');
const bs = require('browser-sync').create();
const historyApiFallback = require('connect-history-api-fallback');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const proxy = require('http-proxy-middleware');
const config = require('../config/webpack/webpack.dev.js');

const bundler = webpack(config);

bs.init({
  port: 4000,
  ui: {
    port: 4001
  },
  server: {
    baseDir: 'src',

    middleware: [
      proxy('/v1', {
        target: process.env.STAGE_URL || 'http://159.65.66.180',
        changeOrigin: true,
        ws: true,
        auth: process.env.BASIC_AUTH,
        logLevel: 'debug'
      }),
      historyApiFallback(),
      webpackDevMiddleware(bundler, {
        publicPath: config.output.publicPath,
        logLevel: 'warn',
        stats: {
          assets: false,
          colors: true,
          version: false,
          hash: false,
          timings: false,
          chunks: false,
          chunkModules: false
        }
      }),
      webpackHotMiddleware(bundler, {
        reload: true
      })
    ]
  },
  files: [
    'src/*.html',
    'locales/**/*.yml'
  ]
});
