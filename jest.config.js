module.exports = {
  setupFiles: [
    "<rootDir>/config/enzyme/enzyme.setup.js"
  ],
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  },
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$',
  moduleFileExtensions: [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json",
    "node"
  ],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/config/jest/fileMock.js',
    '\\.(css|sss)$': 'identity-obj-proxy'
  },
  globals: {
    'ts-jest': {
      tsConfigFile: "./config/jest/tsconfig.jest.json"
    }
  }
}